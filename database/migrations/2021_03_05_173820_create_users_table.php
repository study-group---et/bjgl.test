<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id')->default(0)->comment('角色ID');
            $table->string('user_name', 20)->comment('账号');
            $table->string('truename', '50')->default('未知')->comment('真实姓名');
            $table->string('password', 255)->comment('密码');
//            $table->integer('class_id', 2)->comment('班级所属id');
            $table->enum('sex', ['先生', '女士'])->default('先生')->comment('性别');
            $table->char('last_ip', 15)->default('')->comment('登录ip');
            $table->string('add_user', 20)->default('')->comment('添加人');
            $table->timestamps();
            //软删除
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
