<?php

namespace App\Providers;

use App\Models\Manage;
use App\Models\Node;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            if (\Session::get('user')) {
                $user = Manage::where('username', \Session::get('user')->username)->first();
                $role = $user->role;
                $node = $role->nodes()->pluck('name', 'id')->toArray();
                $menuDate = (new Node())->treeDate(array_keys($node));
                view()->share('menuDate', $menuDate);
            }


//            $view->with('your_var',  );
        });

    }
}
