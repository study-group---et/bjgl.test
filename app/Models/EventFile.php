<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class EventFile extends Model
{
    //1.关联的数据表
    public $table = 'file';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function class()
    {
        return $this->belongsTo(Classname::class, 'class_id');
    }

    public function getClassNameAttribute($key)
    {
        return $this->class['name'];

    }

    public function week()
    {
        return $this->belongsTo(Week::class, 'week_id');
    }

    public function getTermNameAttribute($key)
    {
        return $this->term['title'];

    }

    public function term()
    {
        return $this->belongsTo(Term::class, 'term_id');
    }

    public function getWeekNameAttribute($key)
    {
        return '第' . $this->week['xh'] . '周';

    }

    //访问器 menu Menu
    public function getEventFileAttribute($value)
    {
        $arr = explode('|', $this->file);
//        return $this->file;
        if (is_array($arr)) {
            $array = [];
            foreach ($arr as $v) {
                $arr2 = explode('.', $v);
                $arr3 = explode('/', $arr2[0]);
                $num = count($arr3);
                $array[] = Crypt::decrypt($arr3[$num - 1]) . "<a class='red' href=/" . $v . ">" . "下载" . "</a>";
            }
            return $array;
        }
    }

    public function getFilesAttribute($value)
    {
        $arr = explode('|', $this->file);
//        return $this->file;
        if (is_array($arr)) {
            $array = [];
            foreach ($arr as $v) {
                $arr2 = explode('.', $v);
                $arr3 = explode('/', $arr2[0]);
                $num = count($arr3);
                $array[] = Crypt::decrypt($arr3[$num - 1]) . "<input type='hidden' name='file[]' value='$v'>";
            }
            return $array;
        }
    }

    protected $appends = ['classname', 'weekname', 'eventfile', 'files', 'termname'];
}
