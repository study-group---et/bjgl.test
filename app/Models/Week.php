<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    //
    //1.关联的数据表
    public $table = 'week';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function term()
    {
        return $this->belongsTo(Term::class, 'term_id');
    }

    //访问器 menu Menu
    public function getTermNameAttribute($value)
    {
        return $this->term['title'];
    }

    protected $appends = ['termname'];
}
