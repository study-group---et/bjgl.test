<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stu extends Model
{
    //1.关联的数据表
    public $table = 'stu';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    //访问器 menu Menu
    public function getRoleNameAttribute($value)
    {
        return $this->role['name'];
    }

    public function class()
    {
        return $this->belongsTo(Classname::class, 'class_id');
    }

    public function getClassNameAttribute($key)
    {
        return $this->class['name'];

    }

    protected $appends = ['classname', 'rolename'];
}
