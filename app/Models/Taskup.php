<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Taskup extends Model
{
    //1.关联的数据表
    public $table = 'task_up';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function getTagAttribute($value)
    {
        if ($this->audit_tag == '0') {
            return "<a class='btn btn-xs btn-danger'>未通过</a>";
        } else {
            return "<a class='btn btn-xs btn-success'>已通过<a>";
        }

    }

    public function getUpAttribute($value)
    {
        if ($this->up_tag == '0' || $this->up_tag == null) {
            return "<a class='btn btn-xs btn-danger'>已审核</a>";
        } else {
            return "<a class='btn btn-xs btn-success'>未审核<a>";
        }

    }

//访问器 menu Menu
    public function getFileUpAttribute($value)
    {
        $arr = explode('|', $this->file);
//        return $this->file;
        if (is_array($arr)) {
            $array = [];
            foreach ($arr as $v) {
                $arr2 = explode('.', $v);
                $arr3 = explode('/', $arr2[0]);
                $num = count($arr3);
                $array[] = Crypt::decrypt($arr3[$num - 1]) . "<a class='red' href=/" . $v . ">" . "下载" . "</a>";
            }
            return $array;
        }
    }

    public function stu()
    {
        return $this->belongsTo(Manage::class, 'stu_id');
    }

    //访问器 menu Menu
    public function getStuNameAttribute($value)
    {
        return $this->stu['username'];
    }

    protected $appends = ['tag', 'fileup', 'stuname', 'up'];
}
