<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Task extends Model
{
    //1.关联的数据表
    public $table = 'task_down';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function term()
    {
        return $this->belongsTo(Term::class, 'term_id');
    }

    public function getTitlesAttribute($key)
    {
        return $this->term['title'];

    }

    public function getRenAttribute($value)
    {
        $arr = explode(',', $this->manage_id);
        return count($arr);
    }

    public function week()
    {
        return $this->belongsTo(Week::class, 'week_id');
    }

    public function getWeekNameAttribute($key)
    {
        return '第' . $this->week['xh'] . '周';

    }

    public function getFileUpAttribute($value)
    {
        $arr = explode('|', $this->file);
//        return $this->file;
        if (is_array($arr)) {
            $array = [];
            foreach ($arr as $v) {
                $arr2 = explode('.', $v);
                $arr3 = explode('/', $arr2[0]);
                $num = count($arr3);
                $array[] = Crypt::decrypt($arr3[$num - 1]) . "<a class='red' href=/" . $v . ">" . "下载" . "</a>";
            }
            return $array;
        }
    }

    protected $appends = ['titles', 'weekname', 'ren', 'fileup'];
}
