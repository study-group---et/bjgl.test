<?php

namespace App\Models;


class Role extends Base
{
    public function nodes()
    {
        return $this->belongsToMany('App\Models\Node', 'role_node', 'role_id', 'node_id');
    }
}
