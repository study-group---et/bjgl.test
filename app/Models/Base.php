<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//软删除
use Illuminate\Database\Eloquent\SoftDeletes;

class Base extends Model
{
    //r软删除
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $guarded = [];

    //数据多层级
    public function subTree(array $data, int $pid = 0)
    {
        $arr = [];
        foreach ($data as $val) {
            if ($pid == $val['pid']) {
                $val['sub'] = $this->subTree($data, $val['id']);
                $arr[] = $val;
            }
        }
        return $arr;
    }

}
