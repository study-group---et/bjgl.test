<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuperLog extends Model
{
    //1. 用户模型关联表
    public $table = 'users_log';
    //2.关联表的主键
    public $primaryKey = 'id';
    /**
     * 3.允许被批量操作的字段
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password',
    ];
    //全部批量赋值：没有不允许的，
    //public $guarded = [];

    //4.禁用时间戳(再不用created_at和updated_at两个字段时)
    public $timestamps = false;

    public function getDbLevelAttribute($value)
    {
        if ($value == 1) {
            $rv = '<span class="label label-lg label-info arrowed-in arrowed-in-right">数据添加</span>';
        } elseif ($value == 2) {
            $rv = '<span class="label label-lg label-danger arrowed-in arrowed-in-right">数据删除</span>';
        } elseif ($value == 3) {
            $rv = '<span class="label label-lg label-warning arrowed-in arrowed-in-right">数据更新</span>';
        } elseif ($value == 4) {
            $rv = '<span class="label label-lg label-success arrowed-in arrowed-in-right">数据查询</span>';
        } else {
            $rv = '<kbd>未知操作</kbd>';
        }
        return $rv;
    }

}
