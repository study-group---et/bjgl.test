<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Node extends Base
{
    //修改器  route_name RouteName
    public function setRouteNameAttribute($value)
    {
        $this->attributes['route_name'] = empty($value) ? '' : $value;
    }

    //访问器 menu Menu
    public function getMenuAttribute($value)
    {
        if ($this->is_menu == '1') {
            return "<a class='btn btn-xs btn-success'>是</a>";
        }
        return "<a class='btn btn-xs btn-danger'>否<a>";
    }

//public function
    protected $appends = ['menu'];

    //获取有层级的数据
    public function treeDate(array $allon_node)
    {
//        dump($allon_node);
        $menuDate = Node::orderBy('show_order', 'ASC')->where('is_menu', '1')->whereIn('id', $allon_node)->get()->toArray();
        return $this->subTree($menuDate);
    }
}
