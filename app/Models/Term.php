<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    //
    //1.关联的数据表
    public $table = 'term';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function getShowTagAttribute($key)
    {

        if ($this->tag == '1') {
            return "<a class='btn btn-xs btn-success'>显示</a>";
        }
        return "<a class='btn btn-xs btn-danger'>隐藏<a>";
    }

    protected $appends = ['showtag'];
}
