<?php

namespace App\Models;

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use App\Models\Classname;

class Manage extends Model
{
    //1.关联的数据表
    public $table = 'users';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    //访问器 menu Menu
    public function getRoleNameAttribute($value)
    {
        return $this->role['name'];
    }

    //访问器 menu Menu
    public function getClassNameAttribute($value)
    {
        $arr = explode('|', $this->class_id);
//        return $this->file;
        if (is_array($arr)) {
            $array = [];
            foreach ($arr as $v) {
                $classname = Classname::find($v);
                $array[] = $classname['name'];
            }
            return $array;
        }
    }

    protected $appends = ['rolename', 'classname'];
}
