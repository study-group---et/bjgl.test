<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    //1.关联的数据表
    public $table = 'points';
    //2.主键
    public $primaryKey = 'id';
    //3.允许批量操作的字段
    public $guarded = [];
    //4.是否维护crated_at和updated_at字段
    public $timestamps = false;

    public function class()
    {
        return $this->belongsTo(Classname::class, 'class_id');
    }

    public function getClassNameAttribute($key)
    {
        return $this->class['name'];

    }

    public function stu()
    {
        return $this->belongsTo(Stu::class, 'stu_id');
    }

    public function getStuNameAttribute($key)
    {
        return $this->stu['user_name'];

    }

    public function week()
    {
        return $this->belongsTo(Week::class, 'week_id');
    }

    public function getWeekNameAttribute($key)
    {
        return '第' . $this->week['xh'] . '周';

    }

    protected $appends = ['classname', 'weekname', 'stuname'];
}
