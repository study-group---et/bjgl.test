<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Manage;
use App\Models\Node;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.role.list');
    }

    public function data(Request $request)
    {
        $data = Role::orderBy('created_at', 'DESC')->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $this->validate($request, [
                'name' => 'required'
            ]);
        } catch (\Exception $e) {
            return \redirect()->back()->with('errors', '添加失败！');

        }
        Role::create($request->only('name'));
        return \redirect()->back()->with('success', '恭喜，添加成功！');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::find($id);
        return view('admin.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        try {
            $this->validate($request, [
                'name' => 'required'
            ]);
        } catch (\Exception $e) {
            return \redirect()->back()->with('errors', '修改失败！');

        }
        Role::where('id', $id)->update($request->only('name'));
        return \redirect()->back()->with('success', '恭喜，修改成功！');
    }

    public function auth(Role $role, $id)
    {
        $role = Role::find($id);
        //读取出所有的权限
        $id = Node::orderBy('created_at', 'DESC')->pluck('id')->toArray();
        $quanxian = Node::orderBy('created_at', 'DESC')->get();
        $quanxian2 = Node::orderBy('show_order', 'ASC')->where('pid', 0)->get();
        $node = $role->nodes->pluck('id')->toArray();
        return view('admin.role.auth', compact('role', 'quanxian', 'quanxian2', 'node', 'id'));

    }

    public function doauth(Request $request)
    {
        $role = Role::find($request['role_id']);
        $res = $role->nodes()->sync($request->get('auth'));
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，授权成功！');
        } else {
            return \redirect()->back()->with('errors', '授权失败！');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Role::find($id);
        $res = $role->delete();
        if ($res) {
            return redirect()->back()->with('success', '删除成功');
        } else {
            return redirect()->back()->with('errors', '删除失败');
        }
    }
}
