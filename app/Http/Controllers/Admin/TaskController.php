<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classname;
use App\Models\Manage;
use App\Models\Stu;
use App\Models\Task;
use App\Models\Taskup;
use App\Models\Term;
use App\Models\Week;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use function foo\func;

class TaskController extends Controller
{
    public function Index(Request $request)
    {

        return view('admin.task.list');
    }

    public function Index2(Request $request)
    {

        return view('admin.task.list2');
    }

    public function Index6(Request $request)
    {

        return view('admin.task.list5');
    }

    public function data6(Request $request)
    {
        $date_dang = date('Y-m-d H:i:s');
        $data2 = [];
        $data = Task::orderBy('add_time', 'ASC')->where('time_check', '<', $date_dang)->get();
        foreach ($data as $v) {
            $arr = $v->manage_id;
            $arr = explode(',', $arr);
            foreach ($arr as $key => $value) {
                if (session()->get('user')->id == $value) {
                    $data2 [] = $v;
                }
            }

        }
//        $data2 = Task::orderBy('add_time', 'ASC')->where('id', $v->id)->get();
        return response()->json($data2)->setEncodingOptions(JSON_UNESCAPED_UNICODE);

    }

    public function show(Request $request)
    {
        return view('admin.task.list3');
    }

    public function end(Request $request, $id)
    {
        $date = date('Y-m-d H:i:s');
        return view('admin.task.list4', compact('id', 'date'));

    }

    public function toview($id)
    {
        $task = Task::find($id);
        return view('admin.task.toview', compact('task'));
    }

    public function toview2($id)
    {
        $task = Task::find($id);
        return view('admin.task.toview2', compact('task'));
    }

    public function toview3($id)
    {
        $task = Task::find($id);
        return view('admin.task.toview3', compact('task'));
    }

    public function doview($id)
    {
        $data = Taskup::where('task_id', $id)->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function doend(Request $request)
    {
        $input = $request->except('_token');
        if ($input['file'] != "") {
            $file = implode(',', $input['file']);
        } else {
            $file = "";
        }

        //3.添加到数据库
        $res = Taskup::create(['task_id' => $input['task_id'], 'stu_id' => session()->get('user')->id, 'up_tag' => '1', 'bz' => $input['cont'], 'file' => $file, 'time_check' => $input['time_check'], 'time_up' => date('Y-m-d H:i:s'), 'audit_tag' => 0, 'add_time' => date('Y-m-d H:i:s'), 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }

    }

    public
    function data()
    {
        $date_dang = date('Y-m-d H:i:s');
        $data = Task::orderBy('add_time', 'ASC')->where('time_check', '>', $date_dang)->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public
    function data2(Request $request, $id)
    {
        $data = Taskup::orderBy('add_time', 'ASC')->where('task_id', '=', $id)->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function data3(Request $request)
    {
        $date_dang = date('Y-m-d H:i:s');
        $data = Task::orderBy('add_time', 'ASC')->where('time_check', '<', $date_dang)->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function data4(Request $request)
    {
        $date_dang = date('Y-m-d H:i:s');
        $data2 = [];
        $data = Task::orderBy('add_time', 'ASC')->where('time_check', '>', $date_dang)->get();
        foreach ($data as $v) {
            $arr = $v->manage_id;
            $arr = explode(',', $arr);
            foreach ($arr as $key => $value) {
                if (session()->get('user')->id == $value) {
                    $data2 [] = $v;
                }
            }

        }
//        $data2 = Task::orderBy('add_time', 'ASC')->where('id', $v->id)->get();
        return response()->json($data2)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function data5(Request $request, $id)
    {
        $data = Taskup::orderBy('add_time', 'ASC')->where('stu_id', session()->get('user')->id)->where('task_id', $id)->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }


    public function Create()
    {
        $stu_data = [

            'text' => 'Parent 1',
            'href' => '#parent1',
            'tags' => ['4'],

        ];

        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week = Week::all()->where('term_id', '=', $term_id);
        return view('admin.task.create', compact('week', 'title', 'stu_data'));
    }

    public function Create2()
    {
        $stu_data = [

            'text' => 'Parent 1',
            'href' => '#parent1',
            'tags' => ['4'],

        ];

        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week = Week::all()->where('term_id', '=', $term_id);
        return view('admin.task.create', compact('week', 'title', 'stu_data'));
    }

    public function Update($id)
    {
        $stu = Stu::find($id);
        return view('admin.task.edit', compact('stu'));
    }

    public function Edit(Request $request)
    {
        $id = $request->input('uid');
        $term = Term::find($id);
        $title = $request->input('title');
        $bz = $request->input('bz');
        $time_start = $request->input('start');
        $time_over = $request->input('end');
        $update_time = Carbon::now()->toDateTimeString();
        $term->title = $title;
        $term->time_start = $time_start;
        $term->time_over = $time_over;
        $term->bz = $bz;
        $term->update_time = $update_time;
        $res = $term->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    public function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->except('_token');
        //2.进行表单验证
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $manage_id = $input['data'];
        if (!empty($manage_id)) {
            $number_id = implode(',', $manage_id);
        } else {

            return \redirect()->back()->with('errors', '成员不能为空！');

        }
        if ($input['file'] != "") {
            $file = implode('|', $input['file']);
        } else {
            $file = "";
        }

        //3.添加到数据库
        $time = Carbon::now()->toDateTimeString();
        $res = Task::create(['week_id' => $input['week_id'], 'term_id' => $term_id, 'manage_id' => $number_id, 'title' => $input['title'], 'cont' => $input['bz'], 'file' => $file, 'time_check' => $input['time_check'], 'time_show' => $input['time_show'], 'time_up' => $input['time_up'], 'add_time' => $time, 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public function Del($id)
    {
        $task = Task::find($id);
        $res = $task->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }


    public function check($id)
    {
        return view('admin.task.check', compact('id'));
    }

    public function docheck(Request $request)
    {
        $id = $request->input('id');
        $taskup = Taskup::find($id);
        $audit_tag = $request->input('is_menu');
        $taskup->audit_tag = $audit_tag;
        $res = $taskup->save();
        if ($res) {
            return \redirect()->back()->with('success', '审核通过！');
        } else {
            return \redirect()->back()->with('errors', '审核未通过！');
        }
    }

    public function tree()
    {


        $manage = Manage::select('id', 'username as title')->where('id', '!=', session()->get('user')->id)->get()->toArray();
        $stu = Stu::select('id', 'user_name as title')->get()->toArray();

//        $data = [$manage_data];
        return $manage;
    }

    public function upload(Request $request)
    {
        $file = $request->file('file');//获取文件，与layui中的js的配置 field:'file'。一致。
        $allowed_extensions = ["txt", "doc", "docx", "html", "sql", "xlsx", "xls", "ppt", "pdf"]; //多类型
        //判断文件是否是允许上传的文件类型
        if ($file->getClientOriginalExtension() && !in_array($file->getClientOriginalExtension(), $allowed_extensions)) {
            $data = [
                'status' => 0,//返回状态码，在js中用改状态判断是否上传成功。

            ];
            return json_encode($data);
        }

        //保存文件，新建路径，拷贝文件
        $path = date('Y/m/d/', time());
        $destinationPath = 'uploads/' . $path;
        is_dir($destinationPath) or mkdir($destinationPath, 0777, true);
        $extension = $file->getClientOriginalExtension();
        $fileName = Crypt::encrypt($file->getClientOriginalName()) . '.' . $extension;
        $file->move($destinationPath, $fileName);
        $data = [
            'status' => 1,//返回状态码，在js中用改状态判断是否上传成功。
            'msg' => $destinationPath . $fileName,//上传成功，返回服务器上文件名字
            'originalname' => $file->getClientOriginalName()//上传成功，返回上传原文件名字
        ];
        return json_encode($data);


    }
}
