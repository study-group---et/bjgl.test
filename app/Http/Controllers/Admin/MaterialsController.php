<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Files;
use App\Models\Event;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;

class MaterialsController extends Controller
{
    public function index($id){
        $show_order = Files::max('show_order') + 1;
        $event = Event::find($id);
        return view('admin.materials.materials',compact('show_order','event'));

    }
    public function update($id){
        // $show_order = Files::max('show_order') + 1;
        $files = Files::find($id);
        $event = Event::find($files->event_id);
        return view('admin.materials.edit',compact('files','event'));

    }
    public function edit(Request $request,$id){
        if ($request->input('file') != "") {
            $file = implode('|', $request->input('file'));
        } else {
            $file = "";
        }
//        dd($file);
        $files = Files::find($id);
        $update_time = Carbon::now()->toDateTimeString();
        $files->file = $file;
        $files->update_time = $update_time;
        $res = $files->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }
    public function data($id){
        $data = Files::orderBy('show_order','ASC')->where('event_id',$id)->get();
        // dd($data);
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);

    }
    public function add(Request $request,$id){
        $input = $request->except('_token');
        if ($input['file'] != "") {
            $file = implode('|', $input['file']);
        } else {
            $file = "";
        }
        $time = Carbon::now()->toDateTimeString();
        $res = Files::create(['week_id' => $input['week_id'],  'class_id' => $input['class_id'], 'term_id' => $input['term_id'], 'show_order' => $input['show_order'],'event_id'=>$id, 'file' => $file, 'add_time' => $time, 'add_user' => session()->get('user')->username]);
        // dd($request);
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }

    }
    public function upload(Request $request)
    {

        $file = $request->file('file');//获取文件，与layui中的js的配置 field:'file'。一致。
        $allowed_extensions = ["txt", "doc", "docx", "html", "sql", "xls", "ppt", "pdf","xlsx"]; //多类型
        //判断文件是否是允许上传的文件类型
        if ($file->getClientOriginalExtension() && !in_array($file->getClientOriginalExtension(), $allowed_extensions)) {
            $data = [
                'status' => 0,//返回状态码，在js中用改状态判断是否上传成功。

            ];
            return json_encode($data);
        }

        //保存文件，新建路径，拷贝文件
        $path = date('Y/m/d/', time());
        $destinationPath = 'materials/' . $path;
        is_dir($destinationPath) or mkdir($destinationPath, 0777, true);
        $extension = $file->getClientOriginalExtension();

        $fileName = Crypt::encrypt($file->getClientOriginalName()) . '.' . $extension;
        $file->move($destinationPath, $fileName);
        $data = [
            'status' => 1,//返回状态码，在js中用改状态判断是否上传成功。
            'msg' => $destinationPath . $fileName,//上传成功，返回服务器上文件名字
            'originalname' => $file->getClientOriginalName()//上传成功，返回上传原文件名字
        ];
        return json_encode($data);


    }
    public function Del($id)
    {
        $file = Files::find($id);
        $res = $file->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }
}
