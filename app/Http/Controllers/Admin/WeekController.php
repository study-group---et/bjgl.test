<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Term;
use App\Models\Week;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WeekController extends Controller
{
    //
    public function Index()
    {
        return view('admin.week.list');
    }

    public function data()
    {
        $data = Week::orderBy('term_id', 'ASC')->orderBy('xh', 'ASC')->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function Create()
    {
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week1 = Week::where('term_id', $term_id)->max("xh");
        //判断是否第一次添加
        if ($week1) {
            $where['xh'] = $week1;
            $where['term_id'] = $term_id;
            $start_at1 = Week::where($where)->value('time_start');
            $end_at1 = Week::where($where)->value('time_over');
            $week2 = $week1 + 1;
            $time_start = date("Y-m-d", strtotime("+1 week", strtotime($start_at1)));
            $time_over = date("Y-m-d", strtotime("+1 week", strtotime($end_at1)));
        } else {
            $week2 = 1;
            $time_start = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('time_start');
            $time_over = date("Y-m-d", strtotime("+6 day", strtotime($time_start)));
        }
        return view('admin.week.create', compact('week2', 'time_start', 'time_over', 'title', 'term_id'));
    }

    public function Update($id)
    {
        $week = Week::find($id);
        return view('admin.week.edit', compact('week'));
    }

    public function Edit(Request $request, $id)
    {
        $week = week::find($id);
        $xh = $request->input('xh');
        $bz = $request->input('bz');
        $time_start = $request->input('time_start');
        $time_over = $request->input('time_end');
        $update_time = Carbon::now()->toDateTimeString();
        $week->xh = $xh;
        $week->time_start = $time_start;
        $week->time_over = $time_over;
        $week->update_time = $update_time;
        $res = $week->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    public function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->all();
//        return $input;
        //2.进行表单验证
        //3.添加到数据库
        $time = Carbon::now()->toDateTimeString();
        $res = Week::create(['term_id' => $input['term_id'], 'xh' => $input['xh'], 'time_start' => $input['start_time'], 'time_over' => $input['end_time'], 'add_time' => $time, 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public function Del($id)
    {
        $week = Week::find($id);
        $res = $week->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }
}
