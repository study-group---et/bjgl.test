<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classname;
use App\Models\Manage;
use App\Models\Points;
use App\Models\Stu;
use App\Models\Task;
use App\Models\Term;
use App\Models\Week;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PointsController extends Controller
{
    public function Index(Request $request)
    {
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;

        $class_id = explode('|', $class_id);
//        dd($class_id);
        if ($class_id[0] != "") {
//            dd(1);
            $class = Classname::whereIn('id', $class_id)->get();
            $cont = Classname::whereIn('id', $class_id)->count();
        } else {
            $class = Classname::all();
            $cont = Classname::count();
        }
        $week = Week::where('term_id', $term_id)->get();
        return view('admin.points.list', compact('class', 'week', 'cont'));
    }

    public function data(Request $request)
    {

        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        $input = $request->all();
        if (!empty($class_id[0])) {
            if (!empty($input['week_id']) || !empty($input['class_id'])) {
                if (!empty($input['week_id']) && !empty($input['class_id'])) {
                    $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('class_id', $input['class_id'])->where('score', '>', '0')->get();
                } else if (!empty($input['week_id'])) {
                    $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('score', '>', '0')->get();
                } else if (!empty($input['class_id'])) {
                    $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('class_id', $input['class_id'])->where('score', '>', '0')->get();
                }
            } else {
                $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('score', '>', '0')->get();
            }
        } else {
            if (!empty($input['week_id']) || !empty($input['class_id'])) {
                if (!empty($input['week_id']) && !empty($input['class_id'])) {
                    $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('class_id', $input['class_id'])->where('score', '>', '0')->get();
                } else if (!empty($input['week_id'])) {
                    $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('score', '>', '0')->get();
                } else if (!empty($input['class_id'])) {
                    $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('class_id', $input['class_id'])->where('score', '>', '0')->get();
                }
            } else {
                $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('score', '>', '0')->get();
            }
        }


        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function data2(Request $request)
    {
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        $input = $request->all();
        if (!empty($class_id[0])) {
            if (!empty($input['week_id']) || !empty($input['class_id'])) {
                if (!empty($input['week_id']) && !empty($input['class_id'])) {
                    $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('class_id', $input['class_id'])->where('score', '<', '0')->get();
                } else if (!empty($input['week_id'])) {
                    $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('score', '<', '0')->get();
                } else if (!empty($input['class_id'])) {
                    $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('class_id', $input['class_id'])->where('score', '<', '0')->get();
                }
            } else {
                $data = Points::whereIn('class_id', $class_id)->where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('score', '<', '0')->get();
            }
        } else {
            if (!empty($input['week_id']) || !empty($input['class_id'])) {
                if (!empty($input['week_id']) && !empty($input['class_id'])) {
                    $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('class_id', $input['class_id'])->where('score', '<', '0')->get();
                } else if (!empty($input['week_id'])) {
                    $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('week_id', $input['week_id'])->where('score', '<', '0')->get();
                } else if (!empty($input['class_id'])) {
                    $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('class_id', $input['class_id'])->where('score', '<', '0')->get();
                }
            } else {
                $data = Points::where('term_id', $term_id)->orderBy('class_id', 'DESC')->orderBy('add_time', 'DESC')->where('score', '<', '0')->get();
            }
        }

        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }


    public function Index2(Request $request)
    {
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;

        $class_id = explode('|', $class_id);
//        dd($class_id);
        if ($class_id[0] != "") {
//            dd(1);
            $class = Classname::whereIn('id', $class_id)->get();
            $cont = Classname::whereIn('id', $class_id)->count();
        } else {
            $class = Classname::all();
            $cont = Classname::count();
        }
        $week = Week::where('term_id', $term_id)->get();
        return view('admin.points.list2', compact('class', 'week', 'cont'));
    }

    public function Create()
    {
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        $current = Carbon::now()->toDateTimeString();
        $current_week = Week::where('time_start', '<', $current)->where('time_over', '>', $current)->value('xh');
        if (empty($class_id['0'])) {
            $cont = Classname::where('id', $class_id)->count();
            $class = Classname::all();
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
        }

        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week = Week::where('term_id', '=', $term_id)->get();
        return view('admin.points.create', compact('class', 'week', 'title', 'cont', 'current_week'));
    }

    public function Create2()
    {
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        $current = Carbon::now()->toDateTimeString();
        $current_week = Week::where('time_start', '<', $current)->where('time_over', '>', $current)->value('xh');
        if (empty($class_id['0'])) {
            $cont = Classname::where('id', $class_id)->count();
            $class = Classname::all();
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
        }
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week = Week::where('term_id', '=', $term_id)->get();
        return view('admin.points.create2', compact('class', 'week', 'title', 'cont', 'current_week'));
    }

    public function Update($id)
    {
        $points = Points::find($id);
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (empty($class_id['0'])) {
            $cont = Classname::count();
            $class = Classname::all();
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
        }
        $username = Stu::where('class_id', $points->class_id)->get();
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week = Week::where('term_id', '=', $points->term_id)->get();
        return view('admin.points.edit', compact('points', 'class', 'title', 'week', 'username', 'cont'));
    }

    public function Update2($id)
    {

        $points = Points::find($id);
        $username = Stu::where('class_id', $points->class_id)->get();
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (empty($class_id['0'])) {
            $cont = Classname::count();
            $class = Classname::all();
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
        }
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        $week = Week::where('term_id', '=', $points->term_id)->get();
        return view('admin.points.edit2', compact('points', 'class', 'title', 'week', 'username', 'cont'));
    }

    public function classname(Request $request)
    {
        $input = $request->except('_token');
        $data = "<select name='stu_id'  class='singleSelect width-100'>";
        $week = Stu::find($input->class_id)->get();
        foreach ($week as $v2) {
            $data = $data . "<option value=" . $v2->id . ">";
        }
        $data .= "</selete>";
        return $data;
    }

    public function Edit(Request $request, $id)
    {
        $points = points::find($id);
        $time_happen = $request->input('time_happen');
        $score = $request->input('score');
        $reason = $request->input('reason');
        $class_id = $request->input('class_id');
        $week_id = $request->input('week_id');
        $update_time = Carbon::now()->toDateTimeString();
        $points->time_happen = $time_happen;
        $points->score = $score;
        $points->reason = $reason;
        $points->class_id = $class_id;
        $points->week_id = $week_id;
        $points->update_time = $update_time;
        $res = $points->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    public function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->all();
//        return $input;
        //2.进行表单验证
        //3.添加到数据库
        if (empty($input['hidden'])) {
            $score = $input['score'];
        } else {
            if ($input['score'] > 0) {
                $score = -$input['score'];
            } else {
                $score = $input['score'];
            }
        }
        $time = Carbon::now()->toDateTimeString();
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $res = Points::create(['class_id' => $input['class_id'], 'week_id' => $input['week_id'], 'term_id' => $term_id, 'reason' => $input['reason'], 'stu_id' => $input['username'], 'score' => $score, 'time_happen' => $input['time_happen'], 'add_time' => $time, 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public function Del($id)
    {
        $points = Points::find($id);
        $res = $points->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }

    public function showsite(Request $request)
    {
        $stu = Stu::where('class_id', '=', $request->input('q'))->get();
        $stu_name = "<select lay-verify='required' name='city2' id='city2' lay-filter='xh' onchange='showMsg(this.value)'>";
        $stu_name .= "<option value=0></option>";
        foreach ($stu as $info) {
            $stu_name .= "<option   name=stu_name2 value=" . $info['id'] . ">" . $info['stu_name'] . "</option>";
        }
        $stu_name .= "</select>";
        return $stu_name;
    }

}
