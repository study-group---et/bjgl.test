<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classname;
use App\Models\EventFile;
use App\Models\Manage;
use App\Models\Stu;
use App\Models\Term;
use App\Models\Week;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;

class EventFileController extends Controller
{
    //
    public function Index(Request $request)
    {
        return view('admin.eventfile.list');
    }

    public function data()
    {
        //前台数据
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;
        if (empty($class_id)) {
            $data = EventFile::orderBy('add_time', 'ASC')->get();
        } else {
            $data = EventFile::where('class_id', $class_id)->orderBy('add_time', 'ASC')->get();
        }

        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function Create()
    {
        //添加页
        $class_id = session()->get('user')->class_id;
        if (empty($class_id)) {

            $class = Classname::all();
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $cont = Classname::where('id', $class_id)->count();
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
            $week = Week::all()->where('term_id', '=', $term_id);
        } else {
            $cont = Classname::where('id', $class_id)->count();
            $class = Classname::where('id', $class_id)->get();
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
            $week = Week::all()->where('term_id', '=', $term_id);
        }

        return view('admin.eventfile.create', compact('week', 'title', 'class', 'cont'));
    }

    public function Update($id)
    {
        $class_id = session()->get('user')->class_id;
        if (empty($class_id)) {

            $class = Classname::all();
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $cont = Classname::where('id', $class_id)->count();
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
            $week = Week::all()->where('term_id', '=', $term_id);
        } else {
            $cont = Classname::where('id', $class_id)->count();
            $class = Classname::where('id', $class_id)->get();
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
            $week = Week::all()->where('term_id', '=', $term_id);
        }
        $eventfile = EventFile::find($id);
        return view('admin.eventfile.edit', compact('eventfile', 'class', 'title', 'week', 'cont'));
    }

    public function Edit(Request $request, $id)
    {
//        dd($request->input('file'));
        if ($request->input('file') != "") {
            $file = implode('|', $request->input('file'));
        } else {
            $file = "";
        }
//        dd($file);
        $eventfile = EventFile::find($id);
        $title = $request->input('title');
        $bz = $request->input('bz');
        $update_time = Carbon::now()->toDateTimeString();
        $eventfile->title = $title;
        $eventfile->file = $file;
        $eventfile->bz = $bz;
        $eventfile->update_time = $update_time;
        $res = $eventfile->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    public function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->except('_token');
        if ($input['file'] != "") {
            $file = implode('|', $input['file']);
        } else {
            $file = "";
        }
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        //3.添加到数据库
        $time = Carbon::now()->toDateTimeString();
        $res = EventFile::create(['week_id' => $input['week_id'], 'title' => $input['name'], 'class_id' => $input['class_id'], 'term_id' => $term_id, 'bz' => $input['cont'], 'file' => $file, 'add_time' => $time, 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public function Del($id)
    {
        $eventfile = EventFile::find($id);
        $res = $eventfile->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }

    public function upload(Request $request)
    {

        $file = $request->file('file');//获取文件，与layui中的js的配置 field:'file'。一致。
        $allowed_extensions = ["txt", "doc", "docx", "html", "sql", "xls", "ppt", "pdf"]; //多类型
        //判断文件是否是允许上传的文件类型
        if ($file->getClientOriginalExtension() && !in_array($file->getClientOriginalExtension(), $allowed_extensions)) {
            $data = [
                'status' => 0,//返回状态码，在js中用改状态判断是否上传成功。

            ];
            return json_encode($data);
        }

        //保存文件，新建路径，拷贝文件
        $path = date('Y/m/d/', time());
        $destinationPath = 'eventfile/' . $path;
        is_dir($destinationPath) or mkdir($destinationPath, 0777, true);
        $extension = $file->getClientOriginalExtension();

        $fileName = Crypt::encrypt($file->getClientOriginalName()) . '.' . $extension;
        $file->move($destinationPath, $fileName);
        $data = [
            'status' => 1,//返回状态码，在js中用改状态判断是否上传成功。
            'msg' => $destinationPath . $fileName,//上传成功，返回服务器上文件名字
            'originalname' => $file->getClientOriginalName()//上传成功，返回上传原文件名字
        ];
        return json_encode($data);


    }
}
