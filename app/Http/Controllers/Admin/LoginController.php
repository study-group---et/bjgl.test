<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Manage;
use App\Models\Node;
use App\Models\Term;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    public function login()
    {
        return view('admin.login');
    }

    public function dologin(Request $request)
    {
        $input = $request->except('_token');
        //表单验证

        $rule = [
            'username' => 'required|between:3,18',
            'password' => 'required|between:4,18|alpha_dash',
            'captcha' => 'required|captcha'

        ];
        $msg = [
            'username.required' => '用户名必须输入',
            'username.between' => '用户名长度必须在3-18位之间',
            'password.required' => '密码必须输入',
            'password.between' => '密码长度必须在4-18位之间',
            'password.alpha_dash' => '密码必须是数组字母',
            'captcha.required' => '验证码必须输入',
            'captcha.captcha' => '请重新输入验证码'
        ];

        $validator = \Validator::make($request->all(), $rule, $msg);
        if ($validator->fails()) {
            return redirect('admin/login')
                ->withErrors($validator)
                ->withInput();
        }
        //登录
        //验证用户名
        $user = Manage::where('username', $input['username'])->first();
        if (!$user) {
            return redirect('admin/login')->with('errors', '用户名错误');
        }
        //解密验证密码
        if ($input['password'] != Crypt::decrypt($user->password)) {
            return redirect('admin/login')->with('errors', '密码错误');
        }
        $role = $user->role;
        $path = $role->nodes()->pluck('route_name', 'id');
        $node = $role->nodes()->pluck('name', 'id');
        //权限保存到session
        session()->put('role', $path);
        session()->put('auth', $node);
        //学期存储到session
        $term = Term::where('tag', '1')->get();
        session()->put('term', $term);//put('键','值');
        session()->put('user', $user);//put('键','值');
        $user->times = $user->times + 1;
        $user->last_time = date('Y-m-d H:i:s');
        $user->last_ip = $request->getClientIp();
        $user->save();
        return redirect('admin/index');
    }

    public function index()
    {
        //后台首页展示
        //读取菜单
//        dump(session()->get('auth'));
//        dump($menuDate);
        return view('admin.index');
    }


    public function logout()
    {
        session()->flush();
        return redirect('admin/login');
    }

    public function password()
    {

        return view('admin.pwd.pwd');
    }

    public function dopass(Request $request)
    {
        $manage = Manage::find(session()->get('user')->id);
        $old_password = $request->input('old');
        if ($old_password != Crypt::decrypt($manage->password)) {
            return \redirect()->back()->with('errors', '原密码错误！');
        } else {
            $password = $request->input('re_new');

            $password = Crypt::encrypt($password);
            $manage->password = $password;
//            dd(Crypt::decrypt($password));
            $manage->save();
            session()->flush();
            return redirect("/admin/login")->with('errors', '修改成功,请重新登录!');
        }

    }

//    public function jiami()
//    {
//        dd(Crypt::decrypt('eyJpdiI6ImFhRnNKR2xNM3BDdmxOTUs2WURwTlE9PSIsInZhbHVlIjoiVDBMNFlOZlJUdHRrcTYyWFNGdDBKcXZQNDM4OEd3eXc3Y05TTjZvSUpiVT0iLCJtYWMiOiI5NWNmOTYxMDY5MzM2OWNlOTk5ZDI5NDFhODBiM2U1MjcyMWE1OGFkZTM4NTZlZDg4MjNmYWEzYzdmYjg2YjYwIn0='));
//    }
}
