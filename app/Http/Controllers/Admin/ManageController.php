<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classname;
use App\Models\Manage;
use App\Models\Node;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //获取数据
    public function auth($id)
    {
        $manage = Manage::where('id', $id)->first();
        $roleall = Role::all();
        return view('admin.manage.auth', compact('manage', 'roleall'));
    }

    public function doauth(Request $request)
    {
        $user = Manage::find($request->manage_id);
        if ($request->isMethod('post')) {
            $post = $this->validate($request, ['role_id' => 'required'], ['role_id.required' => '必须选择']);
            $user->update($post);
            return \redirect()->back()->with('success', '恭喜，授权成功！');
        }
        return \redirect()->back()->with('errors', '授权失败！');
    }

    public function index()
    {
        //
        return view('admin.manage.list');
    }

    public function data(Request $request)
    {
        $data = Manage::orderBy('created_at', 'DESC')->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $class = Classname::all();
        return view('admin.manage.create', compact('class'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->all();
        if ($input['pid'] != "") {
            $class_id = implode('|', $input['pid']);
        } else {
            $class_id = "";
        }
//        return $input;
        //2.进行表单验证
        //3.添加到数据库
        $time = Carbon::now()->toDateTimeString();
        $res = Manage::create(['username' => $input['manage_name'], 'password' => Crypt::encrypt($input['password']), 'class_id' => $class_id, 'created_at' => $time, 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $class = Classname::all();
        $manage = Manage::where('id', $id)->first();
        return view('admin.manage.edit', compact('manage', 'class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->input['pid'] != "") {
            $class_id = implode('|', $request->input['pid']);
        } else {
            $class_id = "";
        }
        $mamage = Manage::find($id);
        $mamage_name = $request->input('manage_name');
        $update_time = Carbon::now()->toDateTimeString();
        $mamage->username = $mamage_name;
        $mamage->class_id = $class_id;
        $mamage->updated_at = $update_time;
        $res = $mamage->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
}
