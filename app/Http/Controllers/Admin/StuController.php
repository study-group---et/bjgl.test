<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classname;
use App\Models\Role;
use App\Models\Stu;
use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StuController extends Controller
{
    //
    public function auth($id)
    {
        $stu = Stu::where('id', $id)->first();
        $roleall = Role::all();
        return view('admin.class_stu.auth', compact('stu', 'roleall'));
    }

    public function doauth(Request $request)
    {
        $user = stu::find($request->stu_id);
        if ($request->isMethod('post')) {
            $post = $this->validate($request, ['role_id' => 'required'], ['role_id.required' => '必须选择']);
            $user->update($post);
            return \redirect()->back()->with('success', '恭喜，授权成功！');
        }
        return \redirect()->back()->with('errors', '授权失败！');
    }

    public function Index(Request $request)
    {
        $uid = 0;
        $class_id = session()->get('user')->class_id;
        $class = explode('|', $class_id);
        if ($class[0] == "") {
            $class = Classname::all();
        } else if (count($class) == 1) {
            $uid = 1;
            $class = Classname::whereIn('id', $class)->get();
        } else {
            $class = Classname::whereIn('id', $class)->get();
        }
//        dd($class);
        return view('admin.class_stu.list', compact('class', 'uid'));
    }

    public function data(Request $request)
    {
        // $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        $input = $request->all();
//        dd($input);
        if (!empty($class_id[0])) {
            if (!empty($input['class_id'])) {
                $data = Stu::orderBy('stu_xh', 'ASC')->orwhere('class_id', $input['class_id'])->get();
            } else {
                $data = Stu::whereIn('class_id', $class_id)->orwhere('class_id', $input['class_id'])->orderBy('stu_xh', 'ASC')->get();
            }
        } else {

            if (!empty($input['class_id'])) {
                $data = Stu::orwhere('class_id', $input['class_id'])->orderBy('stu_xh', 'ASC')->get();
            } else {
                $data = Stu::orderBy('stu_xh', 'ASC')->get();
            }
        }
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function Create()
    {
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (empty($class_id)) {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::all();
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
        }
        return view('admin.class_stu.create', compact('class', 'cont'));
    }

    public function Update($id)
    {
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (empty($class_id)) {
            $stu = Stu::find($id);
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::all();
        } else {
            $stu = Stu::find($id);
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
        }


        return view('admin.class_stu.edit', compact('stu', 'class', 'cont'));
    }

    public function Edit(Request $request, $id)
    {
        $stu = Stu::find($id);
        $stu_name = $request->input('stu_name');
        $stu_xh = $request->input('stu_xh');
        $class_id = $request->input('pid');
        $bz = $request->input('bz');
        $update_time = Carbon::now()->toDateTimeString();
        $stu->user_name = $stu_name;
        $stu->class_id = $class_id;
        $stu->bz = $bz;
        $stu->stu_xh = $stu_xh;
        $stu->update_time = $update_time;
        $res = $stu->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    public function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->all();
//        return $input;
        //2.进行表单验证
        //3.添加到数据库
        $time = Carbon::now()->toDateTimeString();
        $res = Stu::create(['user_name' => $input['stu_name'], 'password' => bcrypt($input['password']), 'class_id' => $input['pid'], 'bz' => $input['bz'], 'stu_xh' => $input['stu_xh'], 'add_time' => $time, 'add_user' => session()->get('user')->username]);
//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public function delete($id)
    {
        $stu = stu::find($id);
        $res = $stu->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }
}
