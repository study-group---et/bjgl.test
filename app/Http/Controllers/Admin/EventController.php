<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classname;
use App\Models\Event;
use App\Models\Term;
use App\Models\Files;
use App\Models\Week;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventController extends Controller
{
    //
    public function Index(Request $request)
    {
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $class_id = session()->get('user')->class_id;

        $class_id = explode('|', $class_id);
//        dd($class_id);
        if ($class_id[0] != "") {
//            dd(1);
            $class = Classname::whereIn('id', $class_id)->get();
            $cont = Classname::whereIn('id', $class_id)->count();
        } else {
            $class = Classname::all();
            $cont = Classname::count();
        }
        $week = Week::where('term_id', $term_id)->get();
        return view('admin.event.list', compact('class', 'week', 'cont'));
    }

    public function data(Request $request)
    {
        $input = $request->all();
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (!empty($class_id[0])) {
//            dd($input);
            if (!empty($input['week_id']) || !empty($input['class_id'])) {
                if (!empty($input['week_id']) && !empty($input['class_id'])) {
                    $data = Event::whereIn('class_id', $class_id)->orderBy('add_time', 'ASC')->where('week_id', $input['week_id'])->where('class_id', $input['class_id'])->get();
                } else if (!empty($input['week_id'])) {
                    $data = Event::whereIn('class_id', $class_id)->orderBy('add_time', 'ASC')->where('week_id', $input['week_id'])->get();
                } else if (!empty($input['class_id'])) {
                    $data = Event::whereIn('class_id', $class_id)->orderBy('add_time', 'ASC')->where('class_id', $input['class_id'])->get();
                }
            } else {
                $data = Event::whereIn('class_id', $class_id)->orderBy('add_time', 'ASC')->get();
            }
        } else {
            if (!empty($input['week_id']) || !empty($input['class_id'])) {
                if (!empty($input['week_id']) && !empty($input['class_id'])) {
                    $data = Event::orderBy('add_time', 'ASC')->where('week_id', $input['week_id'])->where('class_id', $input['class_id'])->get();
                } else if (!empty($input['week_id'])) {
                    $data = Event::orderBy('add_time', 'ASC')->where('class_id', $input['class_id'])->get();
                } else if (!empty($input['class_id'])) {
                    $data = Event::orderBy('add_time', 'ASC')->where('week_id', $input['week_id'])->get();
                }
            } else {
                $data = Event::orderBy('add_time', 'ASC')->get();
            }
        }

        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function Create()
    {
        $current = Carbon::now()->toDateTimeString();
        $current_week = Week::where('time_start', '<', $current)->where('time_over', '>', $current)->value('xh');
        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (empty($class_id['0'])) {
            $cont = Classname::count();
            $class = Classname::all();
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
            $week = Week::where('term_id', '=', $term_id)->get();
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
            $week = Week::where('term_id', '=', $term_id)->get();
        }
//        dd($current_week);
        return view('admin.event.create', compact('class', 'week', 'title', 'cont', 'current_week'));
    }

    public function Update($id)
    {


        $class_id = session()->get('user')->class_id;
        $class_id = explode('|', $class_id);
        if (empty($class_id['0'])) {
            $cont = Classname::where('id', $class_id)->count();
            $class = Classname::all();
            $event = Event::find($id);
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $week = Week::where('term_id', '=', $term_id)->get();
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        } else {
            $cont = Classname::whereIn('id', $class_id)->count();
            $class = Classname::whereIn('id', $class_id)->get();
            $event = Event::find($id);
            $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
            $week = Week::where('term_id', '=', $term_id)->get();
            $title = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('title');
        }

        return view('admin.event.edit', compact('event', 'class', 'title', 'week', 'cont'));
    }

    public function Edit(Request $request, $id)
    {
        $event = Event::find($id);
        $rule = [
            'username' => 'required|between:3,18',
            'title' => 'required',
            'class_id' => 'required',
            'week_id' => 'required',
            'time_start' => 'required',
            'time_over' => 'required'

        ];
        $msg = [
            'username.required' => '用户名必须输入',
            'username.between' => '用户名长度必须在3-18位之间',
            'title.required' => '标题必须输入',
            'class_id' => '班级必须选择',
            'captcha.captcha' => '请重新输入验证码'
        ];

        $validator = \Validator::make($request->all(), $rule, $msg);
        if ($validator->fails()) {
            return redirect()
                ->back()->with('errors', '修改失败！');
        }
        $user_name = $request->input('user_name');
        $bz = $request->input('bz');
        $title = $request->input('title');
        $class_id = $request->input('class_id');
        $week_id = $request->input('week_id');
        $time_start = $request->input('start_time');
        $time_over = $request->input('end_time');
        $update_time = Carbon::now()->toDateTimeString();
        $event->user_name = $user_name;
        $event->time_start = $time_start;
        $event->time_over = $time_over;
        $event->class_id = $class_id;
        $event->week_id = $week_id;
        $event->title = $title;
        $event->bz = $bz;
        $event->update_time = $update_time;
        $res = $event->save();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，修改成功！');
        } else {
            return \redirect()->back()->with('errors', '修改失败！');
        }
    }

    public function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->all();
//        return $input;
        //2.进行表单验证
        //3.添加到数据库
        $term_id = Term::where('tag', '1')->orderBy('time_start', 'desc')->value('id');
        $time = Carbon::now()->toDateTimeString();
        $res = Event::create(['user_name' => $input['user_name'], 'title' => $input['title'], 'bz' => $input['bz'], 'class_id' => $input['class_id'], 'week_id' => $input['week_id'], 'term_id' => $term_id, 'time_start' => $input['start_time'], 'time_over' => $input['end_time'], 'add_time' => $time, 'add_user' => session()->get('user')->username]);
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public function Del($id)
    {
        $event = event::find($id);
        $res = $event->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }
}
