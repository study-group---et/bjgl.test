<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Node;
use Illuminate\Http\Request;

class NodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.node.list');
    }

    public function sort()
    {

    }

    public function data(Request $request)
    {

        $data = Node::where('pid', 0)->orderBy('show_order', 'ASC')->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function data2(Request $request)
    {
        $show_order = $request->input('show_order');
        if (!empty($show_order)) {

            $data = Node::orderBy('show_order', 'ASC')->where('pid', $request->up_id)->get();
        } else {
            $data = Node::orderBy('show_order', 'ASC')->where('pid', $request->up_id)->get();
        }

        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //获取所有的顶级
        $show_order = Node::max('show_order') + 1;
        $ding_data = Node::where('pid', 0)->get();
        return view('admin.node.create', compact('ding_data', 'show_order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Node::create($request->except('_token'));
        return \redirect()->back()->with('success', '恭喜，节点添加成功！');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Node $node
     * @return \Illuminate\Http\Response
     */
    public function show(Node $node)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Node $node
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $node = Node::find($id);
        return view('admin.node.edit', compact('node'));
    }

    public function edit2($id)
    {
        //
        $ding_data = Node::where('pid', 0)->get();
        $node = Node::find($id);
        return view('admin.node.edit2', compact('node', 'ding_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Node $node
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $this->validate($request, [
                'name' => 'required'
            ]);
        } catch (\Exception $e) {
            return \redirect()->back()->with('errors', '修改失败！');

        }
        Node::where('id', $id)->update($request->only('name', 'route_name', 'show_order', 'route_class', 'pid', 'is_menu'));
        return \redirect()->back()->with('success', '恭喜，修改成功！');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Node $node
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Node::find($id);

        $res = $role->delete();
        if ($res) {
            return redirect()->back()->with('success', '删除成功');
        } else {
            return redirect()->back()->with('errors', '删除失败');
        }
    }
}
