<?php

namespace App\Http\Controllers\Admin;

use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermController extends Controller
{
    //
    public function Index(Request $request)
    {
//        $term = Term::orderBy('id', 'desc')->where(
//            function ($query) use ($request) {
//                $title = $request->input('title');
//                if (!empty($title)) {
//                    $query->where('title', 'like', '%' . $title . '%');
//                }
//            }
//        )->paginate($request->input('num') ? $request->input('num') : 10);
//        $term = Term::paginate(10);
        return view('admin.term.list');
    }

    public function data()
    {
        $data = Term::orderBy('add_time', 'DESC')->get();
        return response()->json($data)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    public function Create()
    {
        return view('admin.term.create');
    }

    public function Update($id)
    {
        $term = Term::find($id);
        return view('admin.term.edit', compact('term'));
    }

    public function Edit(Request $request, $id)
    {
        $term = Term::find($id);
        $title = $request->input('title');
        $bz = $request->input('bz');
        $time_start = $request->input('time_start');
        $time_over = $request->input('time_end');
        $update_time = Carbon::now()->toDateTimeString();
        $tag = $request->input('show_tag');
        $cont = Term::where('tag', '1')->count();
//        dd($input['show_tag']);
        if ($cont <= 1 || $tag == 2) {
            $term->title = $title;
            $term->time_start = $time_start;
            $term->time_over = $time_over;
            $term->tag = $tag;
            $term->bz = $bz;
            $term->update_time = $update_time;
            $res = $term->save();
            if ($res) {
                return \redirect()->back()->with('success', '恭喜，修改成功！');
            } else {
                return \redirect()->back()->with('errors', '修改失败！');
            }
        } else {
            return \redirect()->back()->with('errors', '学期只能开启一个！');
        }
    }


    public
    function Add(Request $request)
    {
        //1.接受前台提交的表单数据 email pass repass
        $input = $request->all();
//        return $input;
        //2.进行表单验证
        //3.添加到数据库
        $cont = Term::where('tag', '1')->count();
//        dd($input['show_tag']);
        if ($cont == 0 || $input['show_tag'] == 2) {
            $time = Carbon::now()->toDateTimeString();
            $res = Term::create(['title' => $input['title'], 'tag' => $input['show_tag'], 'bz' => $input['bz'], 'time_start' => $input['time_start'], 'time_over' => $input['time_end'], 'add_time' => $time, 'add_user' => session()->get('user')->username]);
        } else {
            return \redirect()->back()->with('errors', '学期只能开启一个！');
        }

//        return $res;
        //4.根据添加是否成功，给客户端返回一个json格式的反馈
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，添加成功！');
        } else {
            return \redirect()->back()->with('errors', '添加失败！');
        }
    }

    public
    function Del($id)
    {
        $term = Term::find($id);
        $res = $term->delete();
        if ($res) {
            return \redirect()->back()->with('success', '恭喜，删除成功！');
        } else {
            return \redirect()->back()->with('errors', '删除失败！');
        }
    }

}
