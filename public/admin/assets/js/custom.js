

var MenuFocus = function () {

	var sidebarActive = function (sidebar, pathname) {
    	var current = sidebar.find('li a[href="' + pathname + '"]');

    	if (current.length) {
            current = current.eq(0);
            var parents = current.eq(0).parents('li');
            var breadcrumb_first = $('.breadcrumbs > ul.breadcrumb > li:eq(0)');
            parents.each(function() {
                var active_a = $(this).children('a')
                var href = active_a.attr('href');
                var text = active_a.text();
                var element = $('<li>' + text + '</li>')
                breadcrumb_first.after(element);
            });
            // alert(parents.length);
    		current.parents('li').addClass('active open');
    		current.parent('li').removeClass('open');

    	}
    };

    var navTabActive = function (nav_tab, pathname) {
        var current = nav_tab.find('li a[href="' + pathname + '"]');

        if (current.length) {
            current = current.eq(0);
            current.attr('href', 'javascript:;')
            var focus = current.parents('li');
            $(".tabbable .nav li").removeClass('active');
            focus.addClass('active');
            focus.removeClass('hidden');

            var breadcrumb = $('.breadcrumbs > ul.breadcrumb');
            var active = $(focus.prop('outerHTML'));
            active.html(active.text());
            breadcrumb.append(active);
        }
    };

    var subNavTabActive = function (sub_nav_tab, pathname) {
        var current = sub_nav_tab.find('li a[href*="' + pathname + '"]');

        if (current.length) {
            current = current.eq(0);
            current.attr('href', 'javascript:;')
            var focus = current.parents('li');
            focus.addClass('active');
            focus.removeClass('hidden');

            var breadcrumb = $('.breadcrumbs > ul.breadcrumb');
            var active = $(focus.prop('outerHTML'));
            active.html(active.text());
            breadcrumb.append(active);
        }else {
            var paramArr = pathname.split('/');
            paramArr.pop();
            subNavTabActive(sub_nav_tab, paramArr.join('/'));
        }
    };

    var topTabActive = function (top_nav_tab, pathname1) {
        var current = top_nav_tab.find('a[href="' + pathname1 + '"]');

        if (current.length) {
            current = current.eq(0);
            $('.dropdown-modal_ a').removeClass('top_active');
          current.addClass('top_active');
        } else {
            var paramArr = pathname1.split('/');
            paramArr.pop();
            topTabActive(top_nav_tab, paramArr.join('/'));
        }
    };



    return {

        init: function () {
            var pathname = window.location.pathname;
            var pathname1 = $.cookie('link');
            var pathname2 = $.cookie('link2');
            var sidebar = $('#sidebar > ul.nav');
            if (sidebar.length) {
                sidebarActive(sidebar, pathname2);
            }

            var nav_tab = $('.tabbable > ul.nav');
            if (nav_tab.length) {
                navTabActive(nav_tab, pathname);
            }

            var sub_nav_tab = $('.sub-tabbable > ul.nav');
            if (sub_nav_tab.length) {
                subNavTabActive(sub_nav_tab, pathname);
            }

            // $(window).resize(function() {
            //   window.location.reload();
            // });

            var top_tab = $('.dropdown-modal_');
            var header_li=top_tab.length-1;
            var header_width;
            header_width=header_li*72+120+162;
            if ($(window).width()<1290) {
                header_width=header_li*72+120;
            }
            $('.wrap_header').width(header_width).css({
                overflow: 'auto'
            });;
            if (top_tab.length) {
                topTabActive(top_tab, pathname1);
            }
        },

    };

}();



jQuery(document).ready(function() {
    MenuFocus.init();
});
