<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                网
            </button>

            <button class="btn btn-info">
                站
            </button>

            <button class="btn btn-warning">
                管
            </button>

            <button class="btn btn-danger">
                理
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="{{ Request::getPathinfo()=='/admin/index'?'active':'' }}">
            <a href="{{url('admin/index')}}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> 欢迎使用 </span>
            </a>

            <b class="arrow"></b>

        </li>
        @if(!empty($menuDate))
            @foreach($menuDate as $item)
                <li class="">
                    <a href="" class="dropdown-toggle">
                        <i class="{{$item['route_class']}}"></i>
                        <span class="menu-text">
								{{$item['name']}}
							</span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>
                    @if(!empty($item['sub']))
                        <ul class="submenu">

                            @foreach($item['sub'] as $subitem)
                                <li class="">
                                    <a href="{{url($subitem['route_name'])}}">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        {{$subitem['name']}}
                                    </a>

                                    <b class="arrow"></b>
                                </li>
                            @endforeach

                        </ul>
                    @endif
                </li>
            @endforeach
        @endif
    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
           data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>
