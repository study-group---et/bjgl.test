<!-- 好用的js日期选择器 -->
<link rel="stylesheet" href="{{asset('admin/assets/Datepicker/css/font-awesome.4.6.0.css')}}">
<link href="{{asset('admin/assets/Datepicker/css/foundation-datepicker.css')}}" rel="stylesheet">
<!-- 好用的js日期选择器 -->
<link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('admin/assets/css/font-awesome.min.css')}}"/>
<!-- page specific plugin styles -->
<!-- text fonts -->
<link rel="stylesheet" href="{{asset('admin/assets/css/ace-fonts.min.css')}}"/>
<!-- ace styles -->
<link rel="stylesheet" href="{{asset('admin/assets/css/ace.min.css')}}" class="ace-main-stylesheet"
      id="main-ace-style"/>
<link rel="stylesheet" href="{{asset('admin/assets/css/ace-skins.min.css')}}"/>
<!--[if lte IE 9]>
			<link rel="stylesheet" href="{{asset('admin/assets/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
		<![endif]-->
<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{asset('admin/assets/css/ace-ie.min.css')}}" />
		<![endif]-->
<!-- inline styles related to this page -->
<link rel="stylesheet" href="{{asset('admin/assets/css/custom.css')}}"/>

<!-- 下拉选择框 -->
<link href="{{asset('admin/assets/serachalbelSelect/searchableSelect.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('admin/assets/layer/css/layui.css')}}" media="all">

<!-- 下拉树插件 -->
<link rel="stylesheet" href="{{asset('admin/assets/proTree/proTree.css')}}"/>
<!-- 图片查看器 -->
<link rel="stylesheet" href="{{asset('admin/assets/magic/jquery.magnify.css')}}"/>
<!-- 下拉框搜索 -->
<link rel="stylesheet" href="{{asset('admin/assets/select-search/select2.min.css')}}"/>

<!--[if lte IE 8]>
		<script src="{{asset('admin/assets/js/html5shiv.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/respond.min.js')}}"></script>
		<![endif]-->
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css')}}"> -->
<link rel="stylesheet" href="{{asset('admin/assets/table/btl.css')}}"/>

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="{{asset('admin/assets/autocomplete/jquery-1.11.2.min.js')}}" type="text/javascript"></script>
<!-- // <script src="{{asset('admin/assets/table/jq.js')}}"></script> -->
<!-- Latest compiled and minified JavaScript -->
<script src="{{asset('admin/assets/table/tbl.js')}}"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js')}}"></script> -->
<!-- Latest compiled and minified Locales -->
<script src="{{asset('admin/assets/table/tbl_cn.js')}}"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/locale/bootstrap-table-zh-CN.min.js')}}"></script> -->
<script src="{{asset('admin/assets/table/export.js')}}"></script>
<!-- <script type="text/javascript" src="http://issues.wenzhixin.net.cn/bootstrap-table/assets/bootstrap-table/src/extensions/export/bootstrap-table-export.js')}}"></script> -->
<script src="{{asset('admin/assets/table/tb_export.js')}}"></script>
<!-- <script type="text/javascript" src="http://rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js')}}"></script> -->
<script src="{{asset('admin/assets/js/jquery-ui.min.js')}}" type="text/javascript"></script>

<!-- 自动完成表单 -->
<!-- <script src="{{asset('admin/assets/autocomplete/jquery.min.js')}}"></script> -->
<link rel="stylesheet" href="{{asset('admin/assets/autocomplete/jquery-ui.css')}}" class="ace-main-stylesheet"
      id="main-ace-style"
/>
<script src="{{asset('admin/assets/autocomplete/jquery-ui.min.js')}}" type="text/javascript"></script>

<!-- Ueditor编辑器 -->
<script type="text/javascript" src="{{asset('admin/assets/Ueditor/ueditor.config.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/Ueditor/ueditor.all.js')}}"></script>

<!-- 下拉选择框 -->
<script type="text/javascript" src="{{asset('admin/assets/serachalbelSelect/jquery.searchableSelect.js')}}"></script>
<!-- 引入laydate js文件  时间选择器 -->
<script src="{{asset('admin/assets/laydate/laydate.js')}}" charset="utf-8"></script>
<script src="{{asset('admin/assets/layui/layui.js')}}" charset="utf-8"></script>
<!-- 引入layer 初始 -->
<script type="text/javascript">
    layui.use('layer', function () {
        var $ = layui.jquery,
            layer = layui.layer;
    })
</script>

<!-- 下拉树 -->
<script src="{{asset('admin/assets/proTree/proTree.js')}}"></script>
<!-- 下拉框搜索 -->
<script src="{{asset('admin/assets/select-search/select2.min.js')}}"></script>

<style>
    #btn_add a {
        color: #333;
    }

    .contMiddle {
        margin-top: 8px;
        word-wrap: break-word;
    }

    .ui-autocomplete-category {
        font-weight: bold;
        padding: .2em .4em;
        margin: .8em 0 .2em;
        line-height: 1.5;
    }

    .ui-autocomplete {
        max-height: 100px;
        overflow-y: auto;
        overflow-x: hidden;
    }

    * html .ui-autocomplete {
        height: 100px;
    }
</style>
