<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/eventfile/index">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            班级大事纪资料列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/eventfile/create">
            班级大事纪资料添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/eventfile/{{ isset($eventfile) ? $eventfile->id : '' }}/update">
            班级大事纪资料编辑
        </a>
    </li>
</ul>
