@extends('layouts.admin')

@section('nav')
    @include('admin/eventfile/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post"
          action="{{url('admin/eventfile/edit',['id'=>$eventfile->id])}}">
        {{ csrf_field() }}
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-xs-12">
                <form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="col-sm-9">

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" style="left: 80px;"
                                   for="form-field-1">学期：{{$title}}</label>
                            <div class="col-sm-4" style="margin-top:7px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">周次：</label>
                            <div class="col-sm-3">
                                <span class="block input-icon input-icon-right">
                                    <select name="week_id" id="ctype_id" class="singleSelect width-100">
                                        <option value="0">事件发生周</option>
                                        @foreach($week as $v2)
                                            <option value="{{$v2->id}}" @if($eventfile->week_id==$v2->id) selected @endif>第{{$v2->xh}}周</option>
                                        @endforeach
                                    </select>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">事件发生班级：</label>

                            <div class="col-xs-12 col-sm-5">
                                <span class="block input-icon input-icon-right">
                                    @if($cont>1)

                                        <select name="class_id" id="ctype_id" class="singleSelect width-100">
                                        <option value="0">请选择班级</option>
                                        @foreach($class as $v)
                                                <option value="{{$v->id}}">{{$v->name}}</option>
                                            @endforeach
                                    </select>
                                    @else
                                        <select name="class_id" id="ctype_id" class="width-100">
                                        @foreach($class as $v)
                                                <option value="{{$v->id}}">{{$v->name}}</option>
                                            @endforeach
                                    </select>
                                    @endif
                                </span>
                            </div>
                            <div class="help-block col-xs-12 col-sm-reset inline red">
                                *(必填)
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">标题：</label>
                            <div class="col-sm-4">
                                <input required type="text" class="col-sm-10" name="title"
                                       value="{{$eventfile->title}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">内容：</label>
                            <div class="col-sm-6">
                                <textarea type="text" class="col-sm-12" rows="15" name="cont" value=""
                                          class="col-xs-5">{{$eventfile->cont}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 文件: </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="layui-upload">
                                            <button type="button" class="layui-btn layui-btn-normal" id="testList">
                                                选择多文件
                                            </button>
                                            <button type="button" class="layui-btn" id="testListAction">开始上传</button>
                                            <div style="color: red">备注：1.可同时选择多个文件，请务必在选择文件后点击“开始上传”按钮！&nbsp;2.文件格式支持图片、word、excel、pdf！
                                            </div>
                                            <div class="layui-upload-list">
                                                <table class="layui-table">
                                                    <thead>
                                                    <th>文件名</th>
                                                    <th>大小</th>
                                                    <th>状态</th>
                                                    <th>操作</th>
                                                    </thead>
                                                    <tbody id="demoList"></tbody>
                                                </table>
                                                <table class="layui-table">
                                                    <caption>已上交文件</caption>
                                                    <thead>
                                                    <th>文件名</th>
                                                    <th>大小</th>
                                                    <th>状态</th>
                                                    <th>操作</th>
                                                    </thead>
                                                    @foreach($eventfile->files as $v)
                                                        <tbody id="demoList">
                                                        <tr>
                                                            <td>{!! $v !!}</td>
                                                            <td></td>
                                                            <td><span style="color: #5FB878;">
                                                                           上传成功</span></td>
                                                            <td>
                                                                <button
                                                                    class="layui-btn layui-btn-xs layui-btn-danger delete">
                                                                    删除
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        </tbody>

                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix form-actions" style="background-color: #fff;border: none;">
                            <div class="col-md-offset-3 col-md-8">
                                <button onclick="return tijiao();" class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    提交
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    重置
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        $('.delete').click(function () {
            var result = confirm('是否删除');
            if (result) {
                //this为点击的当前对象
                //closest('tr')最近的一个<tr></tr>标签,remove()移除
                $(this).closest('tr').remove();
            }
        })
    </script>
    <script>
        layui.use(['form', 'layer'], function () {
            $ = layui.jquery;
            var form = layui.form,
                layer = layui.layer;

            //自定义验证规则
            form.verify({
                nikename: function (value) {
                    if (value.length < 5) {
                        return '昵称至少得5个字符啊';
                    }
                },
                pass: [/(.+){6,12}$/, '密码必须6到12位'],
                repass: function (value) {
                    if ($('#L_pass').val() != $('#L_repass').val()) {
                        return '两次密码不一致';
                    }
                }
            });
        });
        layui.use('upload', function () {
            var $ = layui.jquery,
                upload = layui.upload;
            var demoListView = $('#demoList'),
                uploadListIns = upload.render({
                    elem: '#testList',
                    url: '/admin/eventfile/upload' //改成您自己的上传接口
                    ,
                    accept: 'file',
                    multiple: true,
                    field: 'file',
                    dataType: "json",
                    auto: false,
                    bindAction: '#testListAction',
                    choose: function (obj) {
                        console.log(obj);
                        var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                        //读取本地文件
                        obj.preview(function (index, file, result) {
                            var tr = $(['<tr id="upload-' + index + '">', '<td>' + file.name + '</td>', '<td>' + (file.size / 1024).toFixed(1) + 'kb</td>', '<td>等待上传</td>', '<td>', '<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>', '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>', '</td>', '</tr>'].join(''));

                            //单个重传
                            tr.find('.demo-reload').on('click', function () {
                                obj.upload(index, file);
                            });

                            //删除
                            tr.find('.demo-delete').on('click', function () {
                                delete files[index]; //删除对应的文件
                                tr.remove();
                                uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                            });
                            demoListView.append(tr);
                        });
                    },
                    done: function (res, index, upload) {
                        console.log(res.status);
                        if (res.status) { //上传成功
                            var tr = demoListView.find('tr#upload-' + index),
                                tds = tr.children();
                            tds.eq(2).html('<span style="color: #5FB878;"><input type="hidden" name="file[]" value="' + res.msg + '"/>上传成功</span>');
                            tds.eq(3).html(''); //清空操作
                            return delete this.files[index]; //删除文件队列已经上传成功的文件
                        }
                        this.error(index, upload);
                    },
                    error: function (index, upload) {
                        var tr = demoListView.find('tr#upload-' + index),
                            tds = tr.children();
                        tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                        tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
                    }
                });
        });
    </script>
@endsection
