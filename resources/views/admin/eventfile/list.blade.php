@extends('layouts.admin')

@section('nav')
    @include('admin/eventfile/nav')
@endsection

@section('cont')
    <div id="toolbar" class="btn-group">
        <a href="{{url('admin/eventfile/create')}}" style="color:#333">
            <div id="btn_add" class="btn btn-default">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                新增
            </div>
        </a>
    </div>
    <table id="tb_departments" class="table table-hover table-striped" style="margin-top: 0px;">

    </table>

    <div class="clearfix"></div>
    <script type="text/javascript">
        $(function () {
            //1.初始化Table
            var oTable = new TableInit();
            oTable.Init();

            //2.初始化Button的点击事件
            var oButtonInit = new ButtonInit();
            oButtonInit.Init();

            //3.初始化select的change事件
            $("#zl_export").change(function () {
                $('#tb_departments').bootstrapTable('refreshOptions', {
                    exportDataType: $(this).val()
                });
            });
        });
        var TableInit = function () {
            var oTableInit = new Object();
            //初始化Table
            oTableInit.Init = function () {
                $('#tb_departments').bootstrapTable({
                    url: '{{url('admin/eventfile/data')}}',         //请求后台的URL（*）
                    method: 'get',                      //请求方式（*）
                    toolbar: '#toolbar',                //工具按钮用哪个容器
                    striped: true,                      //是否显示行间隔色
                    cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                    pagination: true,                   //是否显示分页（*）
                    sortable: true,                     //是否启用排序
                    sortOrder: "asc",                   //排序方式
                    queryParams: oTableInit.queryParams,//传递参数（*）
                    sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
                    pageNumber: 1,                       //初始化加载第一页，默认第一页
                    pageSize: 10,                        //每页的记录行数（*）
                    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                    search: true,                       //是否显示表格搜索
                    // strictSearch: false, //是否启用全匹配搜索
                    showColumns: true,                  //是否显示所有的列
                    showRefresh: true,              //是否显示刷新按钮
                    // showPaginationSwitch: true,
                    minimumCountColumns: 2,             //最少允许的列数
                    clickToSelect: true,                //是否启用点击选中行
                    // height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                    uniqueId: "id",                     //每一行的唯一标识，一般为主键列
                    showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
                    cardView: false,                    //是否显示详细视图
                    detailView: false,                   //是否显示父子表
                    clickToSelect: true,
                    showExport: true,
                    exportDataType: "basic",
                    //注册加载子表的事件。注意下这里的三个参数！
                    onExpandRow: function (index, row, $detail) {
                        oTableInit.InitSubTable(index, row, $detail);
                    },
                    onDblClickCell: function (field, value, row, $element) {
                        show(field, value, row, $element);
                    },
                    columns: [
                        {
                            title: '序号',
                            formatter: function (value, row, index) {
                                return index + 1;
                            },
                            sortable: true
                        }, {
                            field: 'termname',
                            title: '学期',
                        }, {
                            field: 'week_id',
                            formatter: function (value, row, index) {

                                return "第" + value + "周";
                            },
                            title: '周次',
                        }, {
                            field: 'classname',
                            title: '班级',
                            sortable: true
                        }, {
                            field: 'title',
                            title: '标题',
                            sortable: true
                        }, {
                            field: 'eventfile',
                            title: '文件',
                            formatter: function (value, row, index) {
                                var a = "";
                                for (i = 0; i < value.length; i++) {
                                    a = a + value[i] + "<br>";
                                }
                                return a;
                            },
                            sortable: true
                        }, {
                            field: 'bz',
                            title: '内容',
                            sortable: true
                        }, {
                            field: 'add_time',
                            title: '添加时间',
                            sortable: true
                        }, {
                            field: 'add_user',
                            title: '添加人',
                            sortable: true
                        }, {
                            field: 'id',
                            formatter: function (value, row, index) {
                                var a = "";
                                a += "<a class='btn btn-xs btn-info' href='/admin/eventfile/" + value + "/update'>编辑</a> ";
                                a += "<a class='btn btn-xs btn-danger' onclick='return del()' href='/admin/eventfile/del/" + value + "'>删除</a>";
                                return a;
                            },
                            title: '操作',
                        },
                    ]
                });
            };

            oTableInit.InitSubTable = function (index, row, $detail) {
                var up_id = row.id;
                var cur_table = $detail.html('<table></table>').find('table');
                $(cur_table).bootstrapTable({
                    url: '{{url('admin/eventfile/data2')}}',         //请求后台的URL（*）
                    method: 'get',
                    contentType: 'application/json;charset=UTF-8',//这里我就加了个utf-8
                    dataType: 'json',
                    queryParams: {up_id: up_id},
                    ajaxOptions: {up_id: up_id},
                    clickToSelect: true,
                    //height: 500,
                    detailView: false,//父子表
                    uniqueId: "id",
                    pageSize: 10,
                    pageList: [10, 25],
                    columns: [
                        {
                            title: '序号',
                            formatter: function (value, row, index) {
                                return index + 1;
                            },
                            sortable: true
                        }, {
                            field: 'stu_id',
                            title: '下发人',
                        }, {
                            field: 'time_check',
                            title: '查看时间',
                        }, {
                            field: 'time_up',
                            title: '上交时间',
                        }, {
                            field: 'file',
                            title: '附件',
                            sortable: true
                        }, {
                            field: 'bz',
                            title: '备注',
                            sortable: true
                        }, {
                            field: 'add_time',
                            title: '添加时间',
                            sortable: true
                        }, {
                            field: 'id',
                            formatter: function (value, row, index) {
                                var a = "";
                                a += "<a class='btn btn-xs btn-info' href='/admin/eventfile/" + value + "/edit2'>编辑</a> ";
                                a += "<a class='btn btn-xs btn-danger' onclick='return del()' href='/admin/eventfile/del/" + value + "'>删除</a>";
                                return a;
                            },
                            title: '操作',
                        },
                    ],
                    //无线循环取子表，直到子表里面没有记录
                    onExpandRow: function (index, row, $Subdetail) {
                        oInit.InitSubTable(index, row, $Subdetail);
                    }
                });
            };
            //得到查询的参数
            oTableInit.queryParams = function (params) {
                var temp = {   //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                    limit: params.limit,   //页面大小
                    offset: params.offset,  //页码
                    departmentname: $("#txt_search_departmentname").val(),
                    statu: $("#txt_search_statu").val()
                };
                return temp;
            };
            return oTableInit;
        };


        var ButtonInit = function () {
            var oInit = new Object();
            var postdata = {};

            oInit.Init = function () {
                //初始化页面上面的按钮事件

            };

            return oInit;
        };

    </script>
@endsection
