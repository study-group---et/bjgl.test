@extends('layouts.admin')

@section('nav')
    @include('admin/node/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/node',['id'=>$node->id])}}">
        {{ csrf_field() }}
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="show_order" class="col-xs-12 col-sm-3 control-label no-padding-right">显示序号：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <input type="number" name="show_order" value="{{ $node->show_order }}" required
                       id="show_order"
                       class="width-100">
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">节点名称：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="name" value="{{$node->name}}" id="name" class="width-100"
                           autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">路由别名：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="route_name" value="{{$node->route_name}}" id="name" class="width-100"
                           autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(填)
            </div>
        </div>
        <div class="form-group" id="route_class" style="display: block;">
            <label for="name"
                   class="col-xs-12 col-sm-3 control-label no-padding-right">列表样式：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="route_class" value="{{$node->route_class}}" id="name" class="width-100"
                           autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(顶级列表必填)
            </div>
        </div>
        <div class="form-group">
            <label for="show_tag" class="col-xs-12 col-sm-3 control-label no-padding-right">是否菜单：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <div class="radio inline">
                       <label>
                           <input name="is_menu" type="radio" value="1" class="ace" @if($node->is_menu==1) checked @endif/>
                           <span class="lbl"> 是</span>
                       </label>
                    <label>
                           <input name="is_menu" type="radio" value="0" class="ace" @if($node->is_menu==0) checked @endif/>
                           <span class="lbl"> 否</span>
                       </label>
                </div>
            </span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">修改</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        //一、对姓名进行验证
        var oldxiaoxi;
        var oldclass;
        //姓名获取焦点时显示，内含失去焦点显示点击前样式及内容
        $("#name").focus(function () {
            oldxiaoxi = $("#xiaoxi").text();
            oldclass = $("#xiaoxi").attr("class");
            //修改css样式为黑色
            $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline");
            //修改HTML内容
            $("#xiaoxi").html("(正在输入...)");
            //当点击完输入框不做操作离开时避免显示正在输入，因此显示点击前所显示的内容及样式
            // $("#name").blur(function() {
            //     //修改css内容
            //     $("#xiaoxi").attr("class",oldclass);
            //     //修改HTML内容
            //     $("#xiaoxi").html(oldxiaoxi);
            // });
        });
        var oldname = "{{$node->name}}";
        var oldxiaoxi1;
        var oldclass1;
        //键盘离开时显示，内含失去焦点显示键盘离开后样式及内容.
        $("#name").keyup(function () {
            var name = $(" #name ").val();
            if (name === '') {
                //修改css样式为黑色
                $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline");
                //修改HTML内容
                $("#xiaoxi").html("(正在输入...)");
            } else if (name === oldname) {
                //修改css样式为黑色
                $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline green");
                //修改HTML内容
                $("#xiaoxi").html("(原用户名)");
            } else {
                //调用ajax
                $.ajax({
                    //请求方式
                    type: "post",

                    //服务器相应数据的解析方式
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //请求资源url
                    url: "/admin/node",
                    //此处需要同步处理，即ajax执行完后才能向下进行。（避免第一次点击输入框只有一次键盘触发时oldxiaoxi为（正在输入...））
                    // async: false,
                    //向服务器端发送的数据
                    data: {"name": name, "ajax": 'cunzai'},
                    success: function (data) {
                        $("#xiaoxi").attr("class", data.newclass);
                        $("#xiaoxi").html(data.cont);
                        oldxiaoxi1 = $("#xiaoxi").text();
                        oldclass1 = $("#xiaoxi").attr("class");
                    }
                });
            }

        });

        //失去焦点时的两种情况，1.获取焦点->失去焦点，2.获取焦点->键盘离开->失去焦点（最后一个else之后便是）
        $("#name").blur(function () {
            if ($(" #name ").val() === '') {
                //修改css样式为黑色
                $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                //修改HTML内容
                $("#xiaoxi").html("*必填");
            } else {
                if (oldxiaoxi1 === "(用户名已存在)") {
                    $("#name").val("");
                    //修改css内容
                    $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                    //修改HTML内容
                    $("#xiaoxi").html("*(必填)");
                } else if (oldxiaoxi1 === "(用户名可用)") {
                    // 修改css内容
                    $("#xiaoxi").attr("class", oldclass1);
                    //修改HTML内容
                    $("#xiaoxi").html(oldxiaoxi1);
                } else {
                    //修改css内容
                    $("#xiaoxi").attr("class", oldclass);
                    //修改HTML内容
                    $("#xiaoxi").html(oldxiaoxi);
                }
            }
        });

    </script>
@endsection
