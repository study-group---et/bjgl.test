@extends('layouts.admin')

@section('nav')
    @include('admin/node/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/node',['id'=>$node->id])}}">
        {{ csrf_field() }}
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="show_order" class="col-xs-12 col-sm-3 control-label no-padding-right">显示序号：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <input type="number" name="show_order" value="{{ $node->show_order }}" required
                       id="show_order"
                       class="width-100">
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">关联分类：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <select name="pid" id="ctype_id" class="singleSelect width-100">
                    <option value="0">==顶级==</option>
                    @foreach($ding_data as $v)
                        <option value="{{$v->id}}" @if($v->id==$node->pid) selected @endif>{{$v->name}}</option>
                    @endforeach
                </select>
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">节点名称：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="name" value="{{$node->name}}" id="name" class="width-100"
                           autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">路由别名：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="route_name" value="{{$node->route_name}}" id="name" class="width-100"
                           autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(填)
            </div>
        </div>
        <div class="form-group">
            <label for="show_tag" class="col-xs-12 col-sm-3 control-label no-padding-right">是否菜单：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <div class="radio inline">
                       <label>
                           <input name="is_menu" type="radio" value="1" class="ace" @if($node->is_menu==1) checked @endif/>
                           <span class="lbl"> 是</span>
                       </label>
                    <label>
                           <input name="is_menu" type="radio" value="0" class="ace" @if($node->is_menu==0) checked @endif/>
                           <span class="lbl"> 否</span>
                       </label>
                </div>
            </span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">修改</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>


    </script>
@endsection
