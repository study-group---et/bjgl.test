@extends('layouts.admin')

@section('nav')
    @include('admin/node/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/node')}}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="show_order" class="col-xs-12 col-sm-3 control-label no-padding-right">显示序号：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <input type="number" name="show_order" value="{{ $show_order }}" required id="show_order"
                       class="width-100">
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">关联分类：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <select name="pid" id="ctype_id" class="singleSelect width-100">
                    <option value="0">==顶级==</option>
                    @foreach($ding_data as $v)
                        <option value="{{$v->id}}">{{$v->name}}</option>
                    @endforeach
                </select>
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">节点名称：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="name" id="name" class="width-100" autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">路由别名：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="route_name" id="name" class="width-100" autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(选填)
            </div>
        </div>
        <div class="form-group" id="route_class" style="display: block;">
            <label for="name"
                   class="col-xs-12 col-sm-3 control-label no-padding-right">列表样式：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="route_class" id="name" class="width-100" autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(顶级列表必填)
            </div>
        </div>
        <div class="form-group">
            <label for="show_tag" class="col-xs-12 col-sm-3 control-label no-padding-right">是否菜单：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <div class="radio inline">
                       <label>
                           <input name="is_menu" type="radio" value="1" class="ace"/>
                           <span class="lbl"> 是</span>
                       </label>
                    <label>
                           <input name="is_menu" type="radio" value="0" class="ace" checked/>
                           <span class="lbl"> 否</span>
                       </label>
                </div>
            </span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    {{--        引入jquery-validation--}}
    <script src="{{url('admin/assets/jquery-validation/lib/jquery.js')}}"></script>
    <script src="{{url('admin/assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
        $("#sample-form").validate({
            errorLabelContainer: "#xiaoxi",
            // 规则
            rules: {
                // 表单元素名称
                name: {
                    // 验证规则
                    required: true
                }
            },
            // 消息提示
            messages: {
                name: {
                    required: '请输入角色名称'
                }
            },
            // 取消键盘事件
            onkeyup: false,
            // 验证成功后的样式
            success: "valid",
            // 验证通过后，处理的方法 form dom对象
            submitHandler: function (form) {
                // 表单提交
                form.submit();
            }
        });
    </script>
    <script>

        var ctype_id = document.getElementById('ctype_id');
        document.addEventListener('click', function () {
            var route_class = document.getElementById('route_class');
            if (ctype_id.value != '0') {
                route_class.style.display = 'none';
            } else {
                route_class.style.display = 'block';
            }
        });


    </script>
@endsection
