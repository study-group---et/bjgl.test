<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/node">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            节点列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/node/create">
            节点添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/node/{{ isset($node) ? $node->id : '' }}/edit">
            节点编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/node/{{ isset($node) ? $node->id : '' }}/edit2">
            节点编辑
        </a>
    </li>
</ul>
