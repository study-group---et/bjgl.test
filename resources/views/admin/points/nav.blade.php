<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/points/index">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            班级加分列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/points/create">
            班级加分添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/points/{{ isset($points) ? $points->id : '' }}/update">
            班级加分编辑
        </a>
    </li>
</ul>
