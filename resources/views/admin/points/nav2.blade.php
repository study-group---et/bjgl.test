<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/points/index2">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            班级减分列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/points/create2">
            班级减分添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/points/{{ isset($points) ? $points->id : '' }}/update2">
            班级减分编辑
        </a>
    </li>
</ul>
