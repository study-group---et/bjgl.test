@extends('layouts.admin')

@section('nav')
    @include('admin/points/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/points/add')}}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">当前学期：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    {{$title}}
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">班级选择：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <select name="class_id" id="class_id" class="width-100">
                    <option value="0">请选择班级</option>
                        @foreach($class as $v)
                        <option value="{{$v->id}}">{{$v->name}}</option>
                    @endforeach
                </select>
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">周次选择：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <select name="week_id" id="week_id" class="singleSelect width-100">
                    <option value="0">事件发生周</option>
                    @foreach($week as $v2)
                        <option value="{{$v2->id}}" @if($current_week==$v2->id) selected @endif>第{{$v2->xh}}周</option>
                    @endforeach
                </select>
            </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">学生姓名：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right" id="user">
                    <input type="text" name="user_name" required="" id="name" disabled class="width-100"
                           autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">发生日期：</label>
            <div class="col-sm-4">
                <input autocomplete="off" type="text" name="time_happen" id="test5" style="height: 35px;"
                       placeholder="yyyy-MM-dd" value="">
                <script src="{$Think.config.super_public_url}/laydate/laydate.js"></script>
                <script>
                    laydate.render({
                        elem: '#test5'
                        , type: 'date'
                    });
                </script>
            </div>
        </div>

        <div class="form-group">
            <label for="remark" class="col-xs-12 col-sm-3 control-label no-padding-right">分值：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="score" id="remark" class="width-100">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline">

            </div>
        </div>
        <div class="form-group">
            <label for="remark" class="col-xs-12 col-sm-3 control-label no-padding-right">加分原因：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="reason" id="remark" class="width-100">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline">

            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        $(function () {
            $("#class_id").click(function () {
                var $class_id = $(this).val();
                if ($class_id == 0) {
                    return;
                } else {
                    $.get("/admin/class_user/" + $class_id, function (data, status) {
                        $('#user').html(data);
                        // console.log(data);
                    });
                }

            })
            // console.log($("#class_id option:selected").val());

        })

    </script>
@endsection
