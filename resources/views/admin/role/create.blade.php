@extends('layouts.admin')

@section('nav')
    @include('admin/role/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/role')}}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label no-padding-right">角色名称：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="text" name="name" id="name" class="width-100" autocomplete="off">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="xiaoxi" class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    {{--        引入jquery-validation--}}
    <script src="{{url('admin/assets/jquery-validation/lib/jquery.js')}}"></script>
    <script src="{{url('admin/assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
        $("#sample-form").validate({
            errorLabelContainer: "#xiaoxi",
            // 规则
            rules: {
                // 表单元素名称
                name: {
                    // 验证规则
                    required: true
                }
            },
            // 消息提示
            messages: {
                name: {
                    required: '请输入角色名称'
                }
            },
            // 取消键盘事件
            onkeyup: false,
            // 验证成功后的样式
            success: "valid",
            // 验证通过后，处理的方法 form dom对象
            submitHandler: function (form) {
                // 表单提交
                form.submit();
            }
        });
    </script>
@endsection
