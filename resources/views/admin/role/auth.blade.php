@extends('layouts.admin')

@section('nav')
    @include('admin/role/nav')
@endsection

@section('cont')
    <form action="{{ url('admin/role/doauth') }}" class="form-horizontal" role="form" method="post">
        {{ csrf_field() }}

        <input type="hidden" name="role_id" value="{{ $role->id }}">

        <table id="simple-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <td style="text-align:left;" colspan="2">
                    用户名： {{ $role->name }}
                </td>
            </tr>
            </thead>

            @foreach($quanxian2 as $cone)
                <tr>
                    <td width="15%" class="left">
                        <span class="lbl">&nbsp;<kbd>{{ $cone->name }}</kbd></span>
                    </td>
                    <td class="ace1">

                        @foreach($quanxian as $ctwo)
                            @if($ctwo->pid==$cone->id)
                                <input name="auth[]" type="checkbox" class="ace" id="auth_one{{ $ctwo->pid }}"
                                       @if(in_array($ctwo->id,$node)) checked @endif value="{{ $ctwo->id }}"/>
                                <label for="auth_one{{ $ctwo->pid }}" class="lbl">{{ $ctwo->name }} </label>
                            @endif
                        @endforeach
                        @if(in_array($cone->id,$id))
                            <input name="auth[]" type="checkbox" class="ace" id="auth_two{{ $cone->id }}"
                                   @if(in_array($cone->id,$node)) checked @endif value="{{ $cone->id }}"/>
                            <label for="auth_two{{ $cone->id }}" class="lbl">{{ $cone->name }}</label>
                        @endif
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="2" width="100%" class="center">
                    <input type="submit" class="btn btn-info"></input>
                </td>
            </tr>
        </table>
    </form>

@endsection
