<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/role">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            角色列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/role/create">
            角色添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/role/{{ isset($role) ? $role->id : '' }}/edit">
            角色编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/role/{{ isset($role) ? $role->id : '' }}/auth">
            管理员授权
        </a>
    </li>
</ul>
