<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/event/index">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            班级大事纪列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/event/create">
            班级大事纪添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/materials/{{ isset($event) ? $event->id : '' }}/create">
            班级大事纪佐证材料添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/materials/{{ isset($files) ? $files->id : '' }}/update">
            班级大事纪佐证材料编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/event/{{ isset($event) ? $event->id : '' }}/update">
            班级大事纪编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/event/{{ isset($event) ? $event->id : '' }}/auth">
            班级大事纪授权
        </a>
    </li>
</ul>
