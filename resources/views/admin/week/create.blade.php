@extends('layouts.admin')

@section('nav')
    @include('admin/week/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/week/add')}}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-12">
                <input type="hidden" name="term_id" value="{{$term_id}}">
                <form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">隶属学期：</label>
                        <div class="col-sm-4">
                            {{$title}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">周次：</label>
                        <div class="col-sm-3">
                            <input type="number" style="height: 35px;" required name="xh" value="{{$week2}}"
                                   class="col-xs-5"/>
                            <span class="red">&nbsp;&nbsp;例如：填写1,即为第一周 （0：表示开学前所有时间）</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">开始日期：</label>
                        <div class="col-sm-4">
                            <input autocomplete="off" type="text" name="start_time" id="test5" style="height: 35px;"
                                   placeholder="yyyy-MM-dd" value="{{$time_start}}">
                            <script src="{$Think.config.super_public_url}/laydate/laydate.js"></script>
                            <script>
                                laydate.render({
                                    elem: '#test5'
                                    , type: 'date'
                                });
                            </script>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">结束日期：</label>
                        <div class="col-sm-4">
                            <input autocomplete="off" type="text" name="end_time" id="test6" style="height: 35px;"
                                   placeholder="yyyy-MM-dd" value="{{$time_over}}">
                            <script>
                                laydate.render({
                                    elem: '#test6'
                                    , type: 'date'
                                });
                            </script>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">备注：</label>
                        <div class="col-sm-6">
                            <input type="text" style="height: 35px;" placeholder="" name="bz" class="col-xs-5"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                            <button class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;
                            <button class="btn btn-warning" type="reset">重置</button>
                        </div>
                    </div>
                </form>
                @endsection

                @section('js')
                    {{--        引入jquery-validation--}}
                    <script src="{{url('admin/assets/jquery-validation/lib/jquery.js')}}"></script>
                    <script src="{{url('admin/assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
                    <script>
                        $("#sample-form").validate({
                            errorLabelContainer: "#xiaoxi",
                            // 规则
                            rules: {
                                // 表单元素名称
                                title: {
                                    // 验证规则
                                    required: true
                                }
                            },
                            // 消息提示
                            messages: {
                                title: {
                                    required: '请输入学期名称'
                                }
                            },
                            // 取消键盘事件
                            onkeyup: false,
                            // 验证成功后的样式
                            success: "valid",
                            // 验证通过后，处理的方法 form dom对象
                            submitHandler: function (form) {
                                // 表单提交
                                form.submit();
                            }
                        });
                    </script>
@endsection
