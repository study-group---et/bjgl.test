@extends('layouts.admin')

@section('nav')
    @include('admin/pwd/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('/admin/dopass')}}">
        {{ csrf_field() }}
        @csrf
        <div class="form-group">
            <label for="password" class="col-xs-12 col-sm-3 control-label no-padding-right">原密码：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="password" name="old" required placeholder="不填写则默认原密码" class="width-100">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-xs-12 col-sm-3 control-label no-padding-right">新密码：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="password" name="new" required placeholder="不填写则默认原密码" id="password" class="width-100">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div id="password_id" class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-xs-12 col-sm-3 control-label no-padding-right">再次新密码：</label>

            <div class="col-xs-12 col-sm-5">
                <span class="block input-icon input-icon-right">
                    <input type="password" name="re_new" required placeholder="不填写则默认原密码" class="width-100">
                    <i class="icon-leaf"></i>
                </span>
            </div>
            <div class="help-block col-xs-12 col-sm-reset inline red">
                *(必填)
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">修改</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        //二、对密码进行验证
        var oldpassword_x;
        var oldpassword_c;
        //姓名获取焦点时显示，内含失去焦点显示点击前样式及内容
        $("#password").focus(function () {
            oldpassword_x = $("#password_id").text();
            oldpassword_c = $("#password_id").attr("class");
            //修改css样式为黑色
            $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline");
            //修改HTML内容
            $("#password_id").html("(请输入6-16位数字、字母或常用符号，字母区分大小写)");
        });


        var qiangdu;
        //键盘离开时
        $('#password').keyup(function () {
            //密码为八位及以上并且字母数字特殊字符三项都包括
            var strongRegex = new RegExp("^(?=.{6,})((?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W))|((?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).*$", "g");
            //密码为七位及以上并且字母、数字、特殊字符三项中有两项，强度是中等
            var mediumRegex = new RegExp("^(?=.{6,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
            //密码为六位及及以上，只含有一种，强度为弱
            var enoughRegex = new RegExp("(?=.{6,}).*", "g");
            if (false == enoughRegex.test($('#password').val())) {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline");
                $("#password_id").html("(请输入6-16位数字、字母或常用符号，字母区分大小写)");
                qiangdu = 0;
            } else if (strongRegex.test($('#password').val())) {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline green");
                $("#password_id").html("(强：您的密码很安全！)");
                qiangdu = 1;
            } else if (mediumRegex.test($('#password').val())) {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline blue");
                $("#password_id").html("(中：您的密码号可以更复杂些！)");
                qiangdu = 2;
            } else {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                $("#password_id").html("(弱：您输入的密码强度过弱，试试字母、数字、常用符号的组合！)");
                qiangdu = 3;
            }
            return true;
        });

        $("#password").blur(function () {
            if (qiangdu == 0) {
                $("#password").val("");
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                $("#password_id").html("*必填");
            } else if (qiangdu == 1) {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline green");
                $("#password_id").html("(强：您的密码很安全！)");
            } else if (qiangdu == 2) {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline blue");
                $("#password_id").html("(中：您的密码号可以更复杂些！)");
            } else if (qiangdu == 3) {
                $("#password_id").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                $("#password_id").html("(弱：您输入的密码强度过弱，试试字母、数字、常用符号的组合！)");
            } else {
                $("#password_id").attr("class", oldpassword_c);
                $("#password_id").html(oldpassword_x);
            }
        });

    </script>
@endsection
