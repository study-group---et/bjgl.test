@extends('layouts.admin')

@section('nav')
    @include('admin/task/nav2')
@endsection

@section('cont')

    <form class="form-horizontal" id="sample-form" method="post" action="{{url('/admin/task/doend')}}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-12">
                <form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">状态：</label>
                            <div class="col-sm-6">
                                准时查看
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">查看时间：</label>
                            <div class="col-sm-6">
                                <input type="hidden" name="task_id" value="{{$id}}">
                                <input type="hidden" name="time_check" value="{{$date}}">
                                {{$date}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">内容：</label>
                            <div class="col-sm-6">
                                <textarea type="text" class="col-sm-12" rows="15" name="cont" value=""
                                          class="col-xs-5"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 文件: </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="layui-upload">
                                            <button type="button" class="layui-btn layui-btn-normal" id="testList">
                                                选择多文件
                                            </button>
                                            <button type="button" class="layui-btn" id="testListAction">开始上传</button>
                                            <div style="color: red">备注：1.可同时选择多个文件，请务必在选择文件后点击“开始上传”按钮！&nbsp;2.文件格式支持图片、word、excel、pdf！
                                            </div>
                                            <div class="layui-upload-list">
                                                <table class="layui-table">
                                                    <thead>
                                                    <th>文件名</th>
                                                    <th>大小</th>
                                                    <th>状态</th>
                                                    <th>操作</th>
                                                    </thead>
                                                    <tbody id="demoList"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix form-actions" style="background-color: #fff;border: none;">
                            <div class="col-md-offset-3 col-md-8">
                                <button onclick="return tijiao();" class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    提交
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    重置
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <div style="text-align: center"><h3>请于任务结束前完成任务</h3></div>
    <table id="tb_departments" class="table table-hover table-striped" style="margin-top: 0px;">

    </table>

    <div class="clearfix"></div>

    <script type="text/javascript">
        $(function () {
            //1.初始化Table
            var oTable = new TableInit();
            oTable.Init();

            //2.初始化Button的点击事件
            var oButtonInit = new ButtonInit();
            oButtonInit.Init();

            //3.初始化select的change事件
            $("#zl_export").change(function () {
                $('#tb_departments').bootstrapTable('refreshOptions', {
                    exportDataType: $(this).val()
                });
            });
        });
        var TableInit = function () {
            var oTableInit = new Object();
            //初始化Table
            oTableInit.Init = function () {
                $('#tb_departments').bootstrapTable({
                    url: '{{url('admin/task/data4')}}',         //请求后台的URL（*）
                    method: 'get',                      //请求方式（*）
                    toolbar: '#toolbar',                //工具按钮用哪个容器
                    striped: true,                      //是否显示行间隔色
                    cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                    pagination: false,                   //是否显示分页（*）
                    sortable: true,                     //是否启用排序
                    sortOrder: "asc",                   //排序方式
                    queryParams: oTableInit.queryParams,//传递参数（*）
                    sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
                    pageNumber: 1,                       //初始化加载第一页，默认第一页
                    pageSize: 10,                        //每页的记录行数（*）
                    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                    search: false,                       //是否显示表格搜索
                    // strictSearch: false, //是否启用全匹配搜索
                    showColumns: false,                  //是否显示所有的列
                    showRefresh: false,              //是否显示刷新按钮
                    // showPaginationSwitch: true,
                    minimumCountColumns: 2,             //最少允许的列数
                    clickToSelect: true,                //是否启用点击选中行
                    // height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                    uniqueId: "id",                     //每一行的唯一标识，一般为主键列
                    showToggle: false,                    //是否显示详细视图和列表视图的切换按钮
                    cardView: false,                    //是否显示详细视图
                    detailView: true,                   //是否显示父子表
                    clickToSelect: true,
                    showExport: false,
                    exportDataType: "basic",
                    //注册加载子表的事件。注意下这里的三个参数！
                    onExpandRow: function (index, row, $detail) {
                        oTableInit.InitSubTable(index, row, $detail);
                    },
                    onDblClickCell: function (field, value, row, $element) {
                        show(field, value, row, $element);
                    },
                    columns: [
                        {
                            title: '序号',
                            formatter: function (value, row, index) {
                                return index + 1;
                            },
                            sortable: true
                        }, {
                            field: 'titles',
                            title: '隶属学期',
                        }, {
                            field: 'weekname',
                            title: '周次',
                            sortable: true
                        }, {
                            field: 'ren',
                            title: '人数',
                            sortable: true
                        }, {
                            field: 'title',
                            title: '标题',
                            sortable: true
                        }, {
                            field: 'time_check',
                            title: '开始时间',
                            sortable: true
                        }, {
                            field: 'fileup',
                            title: '附件',
                            formatter: function (value, row, index) {
                                var a = "";
                                for (i = 0; i < value.length; i++) {
                                    a = a + value[i] + "<br>";
                                }
                                return a;
                            },
                            sortable: true
                        }, {
                            field: 'time_up',
                            title: '结束时间',
                            sortable: true
                        }, {
                            field: 'add_time',
                            title: '添加时间',
                            sortable: true
                        }, {
                            field: 'add_user',
                            title: '添加人',
                            sortable: true
                        },
                    ]
                });
            };

            oTableInit.InitSubTable = function (index, row, $detail) {
                var up_id = row.id;
                var cur_table = $detail.html('<table></table>').find('table');
                $(cur_table).bootstrapTable({
                    url: '/admin/task/data5/' + up_id,         //请求后台的URL（*）
                    method: 'get',
                    contentType: 'application/json;charset=UTF-8',//这里我就加了个utf-8
                    dataType: 'json',
                    queryParams: {up_id: up_id},
                    ajaxOptions: {up_id: up_id},
                    clickToSelect: true,
                    //height: 500,
                    detailView: false,//父子表
                    uniqueId: "id",
                    pageSize: 10,
                    pageList: [10, 25],
                    columns: [
                        {
                            title: '序号',
                            formatter: function (value, row, index) {
                                return index + 1;
                            },
                            sortable: true
                        }, {
                            field: 'stuname',
                            title: '下发人',
                        }, {
                            field: 'time_check',
                            title: '查看时间',
                        }, {
                            field: 'time_up',
                            title: '上交时间',
                        }, {
                            field: 'fileup',
                            title: '附件',
                            sortable: true
                        }, {
                            field: 'bz',
                            title: '备注',
                            sortable: true
                        }, {
                            field: 'tag',
                            title: '审核状态',
                            sortable: true
                        }, {
                            field: 'add_time',
                            title: '添加时间',
                            sortable: true
                        },
                    ],
                    //无线循环取子表，直到子表里面没有记录
                    onExpandRow: function (index, row, $Subdetail) {
                        oInit.InitSubTable(index, row, $Subdetail);
                    }
                });
            };
            //得到查询的参数
            oTableInit.queryParams = function (params) {
                var temp = {   //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                    limit: params.limit,   //页面大小
                    offset: params.offset,  //页码
                    departmentname: $("#txt_search_departmentname").val(),
                    statu: $("#txt_search_statu").val()
                };
                return temp;
            };
            return oTableInit;
        };


        var ButtonInit = function () {
            var oInit = new Object();
            var postdata = {};

            oInit.Init = function () {
                //初始化页面上面的按钮事件

            };

            return oInit;
        };

    </script>
@endsection
@section('js')
    <script>
        layui.use(['form', 'layer'], function () {
            $ = layui.jquery;
            var form = layui.form,
                layer = layui.layer;

            //自定义验证规则
            form.verify({
                nikename: function (value) {
                    if (value.length < 5) {
                        return '昵称至少得5个字符啊';
                    }
                },
                pass: [/(.+){6,12}$/, '密码必须6到12位'],
                repass: function (value) {
                    if ($('#L_pass').val() != $('#L_repass').val()) {
                        return '两次密码不一致';
                    }
                }
            });
        });
        layui.use('upload', function () {
            var $ = layui.jquery,
                upload = layui.upload;
            var demoListView = $('#demoList'),
                uploadListIns = upload.render({
                    elem: '#testList',
                    url: '/admin/task/upload' //改成您自己的上传接口
                    ,
                    accept: 'file',
                    multiple: true,
                    field: 'file',
                    dataType: "json",
                    auto: false,
                    bindAction: '#testListAction',
                    choose: function (obj) {
                        var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                        //读取本地文件
                        obj.preview(function (index, file, result) {
                            var tr = $(['<tr id="upload-' + index + '">', '<td>' + file.name + '</td>', '<td>' + (file.size / 1024).toFixed(1) + 'kb</td>', '<td>等待上传</td>', '<td>', '<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>', '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>', '</td>', '</tr>'].join(''));

                            //单个重传
                            tr.find('.demo-reload').on('click', function () {
                                obj.upload(index, file);
                            });

                            //删除
                            tr.find('.demo-delete').on('click', function () {
                                delete files[index]; //删除对应的文件
                                tr.remove();
                                uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                            });

                            demoListView.append(tr);
                        });
                    },
                    done: function (res, index, upload) {
                        console.log(res.status);
                        if (res.status) { //上传成功
                            var tr = demoListView.find('tr#upload-' + index),
                                tds = tr.children();
                            tds.eq(2).html('<span style="color: #5FB878;"><input type="hidden" name="file[]" value="' + res.msg + '"/>上传成功</span>');
                            tds.eq(3).html(''); //清空操作
                            return delete this.files[index]; //删除文件队列已经上传成功的文件
                        }
                        this.error(index, upload);
                    },
                    error: function (index, upload) {
                        var tr = demoListView.find('tr#upload-' + index),
                            tds = tr.children();
                        tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                        tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
                    }
                });
        });
    </script>
@endsection
