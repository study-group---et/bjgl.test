@extends('layouts.admin')

@section('nav')
    @include('admin/task/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('/admin/task/add')}}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-12">
                <form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="col-sm-9">
                        <input type="hidden" name="teacher_id" value="{:session('s_id')}">

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" style="left: 80px;"
                                   for="form-field-1">学期：{{$title}}</label>
                            <div class="col-sm-4" style="margin-top:7px">
                                <input type="hidden" name="terms_id" value="{$now_term.id}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">周次：</label>
                            <div class="col-sm-3">
                                <span class="block input-icon input-icon-right">
                <select name="week_id" id="ctype_id" class="singleSelect width-100">
                    <option value="0">事件发生周</option>
                    @foreach($week as $v2)
                        <option value="{{$v2->id}}">第{{$v2->xh}}周</option>
                    @endforeach
                </select>
            </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">标题：</label>
                            <div class="col-sm-4">
                                <input required type="text" class="col-sm-10" name="title" value="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">内容：</label>
                            <div class="col-sm-6">
                                <textarea type="text" class="col-sm-12" rows="15" name="bz" value=""
                                          class="col-xs-5"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 文件: </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="layui-upload">
                                            <button type="button" class="layui-btn layui-btn-normal" id="testList">
                                                选择多文件
                                            </button>
                                            <button type="button" class="layui-btn" id="testListAction">开始上传</button>
                                            <div style="color: red">备注：1.可同时选择多个文件，请务必在选择文件后点击“开始上传”按钮！&nbsp;2.文件格式支持图片、word、excel、pdf！
                                            </div>
                                            <div class="layui-upload-list">
                                                <table class="layui-table">
                                                    <thead>
                                                    <th>文件名</th>
                                                    <th>大小</th>
                                                    <th>状态</th>
                                                    <th>操作</th>
                                                    </thead>
                                                    <tbody id="demoList"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="file_src" id="file" value=""/>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">任务显示时间：</label>
                            <div class="col-sm-4">
                                <input required autocomplete="off" type="text" name="time_show" id="test5">
                                <script src="{$Think.config.super_public_url}/laydate/laydate.js"></script>
                                <script>
                                    laydate.render({
                                        elem: '#test5'
                                        , type: 'datetime'
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">查看截止时间：</label>
                            <div class="col-sm-4">
                                <input required autocomplete="off" type="text" name="time_check" id="test6">
                                <script>
                                    laydate.render({
                                        elem: '#test6'
                                        , type: 'datetime'
                                    });
                                </script>
                            </div>
                        </div>
                        <div id="manage"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">上交截止时间：</label>
                            <div class="col-sm-4">
                                <input required autocomplete="off" type="text" name="time_up" id="test7">
                                <script>
                                    laydate.render({
                                        elem: '#test7'
                                        , type: 'datetime'
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="clearfix form-actions" style="background-color: #fff;border: none;">
                            <div class="col-md-offset-3 col-md-8">
                                <button id="save" class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn btn-warning" type="reset">重置</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3" style="margin-left: -100px;">
                        <div class="col-sm-8" style="width: 400px;">
                            <div class="col-sm-12">
                                <p class="form-control-static"><b>成员列表</b></p>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="trees" id="test12" class=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        layui.use(['form', 'layer'], function () {
            $ = layui.jquery;
            var form = layui.form,
                layer = layui.layer;

            //自定义验证规则
            form.verify({
                nikename: function (value) {
                    if (value.length < 5) {
                        return '昵称至少得5个字符啊';
                    }
                },
                pass: [/(.+){6,12}$/, '密码必须6到12位'],
                repass: function (value) {
                    if ($('#L_pass').val() != $('#L_repass').val()) {
                        return '两次密码不一致';
                    }
                }
            });
        });
        layui.use('upload', function () {
            var $ = layui.jquery,
                upload = layui.upload;
            var demoListView = $('#demoList'),
                uploadListIns = upload.render({
                    elem: '#testList',
                    url: '/admin/task/upload' //改成您自己的上传接口
                    ,
                    accept: 'file',
                    multiple: true,
                    field: 'file',
                    dataType: "json",
                    auto: false,
                    bindAction: '#testListAction',
                    choose: function (obj) {
                        var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                        //读取本地文件
                        obj.preview(function (index, file, result) {
                            var tr = $(['<tr id="upload-' + index + '">', '<td>' + file.name + '</td>', '<td>' + (file.size / 1024).toFixed(1) + 'kb</td>', '<td>等待上传</td>', '<td>', '<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>', '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>', '</td>', '</tr>'].join(''));

                            //单个重传
                            tr.find('.demo-reload').on('click', function () {
                                obj.upload(index, file);
                            });

                            //删除
                            tr.find('.demo-delete').on('click', function () {
                                delete files[index]; //删除对应的文件
                                tr.remove();
                                uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                            });

                            demoListView.append(tr);
                        });
                    },
                    done: function (res, index, upload) {
                        console.log(res.status);
                        if (res.status) { //上传成功
                            var tr = demoListView.find('tr#upload-' + index),
                                tds = tr.children();
                            tds.eq(2).html('<span style="color: #5FB878;"><input type="hidden" name="file[]" value="' + res.msg + '"/>上传成功</span>');
                            tds.eq(3).html(''); //清空操作
                            return delete this.files[index]; //删除文件队列已经上传成功的文件
                        }
                        this.error(index, upload);
                    },
                    error: function (index, upload) {
                        var tr = demoListView.find('tr#upload-' + index),
                            tds = tr.children();
                        tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                        tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
                    }
                });
        });
    </script>
    <script>

        layui.use(['tree', 'util', 'layer', 'jquery', 'form'], function () {
            var tree = layui.tree,
                layer = layui.layer,
                util = layui.util,
                $ = layui.jquery,
                form = layui.form;
            $.ajax({
                url: "/admin/task/tree",
                type: "post",
                data: {
                    '_token': '{{ csrf_token() }}'
                },
                dataType: 'json',
                success: function (xhr) {
                    tree.render({
                        elem: '#test12',
                        data: xhr,
                        showCheckbox: true //是否显示复选框
                        ,
                        id: 'demoId1',
                        isJump: true //是否允许点击节点时弹出新窗口跳转
                        ,
                        click: function (obj) {

                        },

                    });
                }
            })
            // 监听点击按钮
            //获取所有选中节点id数组
            // 监听点击按钮
            $("#save").click(function () {
                //获取所有选中节点id数组
                var checkData = tree.getChecked('demoId1');
                var arr = [];
                for (var i in checkData) {
                    arr[arr.length] = checkData[i].id;
                }
                document.getElementById('manage').innerHTML = '<input type="hidden" name="data[]" value="' + arr + '"/>';
            });

            function getChecked_list(data) {
                var id = "";
                $.each(data, function (index, item) {
                    if (id != "") {
                        id = id + "," + item.id;
                    } else {
                        id = item.id;
                    }
                    var i = getChecked_list(item.children);
                    if (i != "") {
                        id = id + "," + i;
                    }
                    console.log(id);
                });
                return id;
            }
        });
    </script>
@endsection
