<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/task/show">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            我的任务
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/create2">
            任务添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/end">
            任务上交
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/check">
            任务审核
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/update">
            任务编辑
        </a>
    </li>
</ul>
