<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/task/index6">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            任务完成列表
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/toview2">
            任务查看
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/create">
            任务添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/check">
            任务审核
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/update">
            任务编辑
        </a>
    </li>
</ul>
