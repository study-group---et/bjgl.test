@extends('layouts.admin')
@section('nav')
    @include('admin/task/nav2')
@endsection
@section('cont')
    <div class="row">
        <div class="col-xs-12">
            <form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">标题：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->title}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">内容：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->cont}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">查看时间：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->time_check}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">截止时间：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->time_up}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">文件：</label>
                        <div class="col-sm-6" style="top:8px">
                            @foreach($task->fileup as $v)
                                {!! $v !!}<br>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">添加人：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->add_user}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">添加时间：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->add_time}}
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <h3 style="text-align: center">已提交列表</h3>
    <table id="tb_departments" class="table table-hover table-striped" style="margin-top: 0px;">

    </table>

    <div class="clearfix"></div>
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {
            //1.初始化Table
            var oTable = new TableInit();
            oTable.Init();

            //2.初始化Button的点击事件
            var oButtonInit = new ButtonInit();
            oButtonInit.Init();

            //3.初始化select的change事件
            $("#zl_export").change(function () {
                $('#tb_departments').bootstrapTable('refreshOptions', {
                    exportDataType: $(this).val()
                });
            });
        });
        var TableInit = function () {
            var oTableInit = new Object();
            //初始化Table
            oTableInit.Init = function () {
                $('#tb_departments').bootstrapTable({
                    url: '/admin/task/doview/' +{{$task->id}},         //请求后台的URL（*）
                    method: 'get',                      //请求方式（*）
                    toolbar: '#toolbar',                //工具按钮用哪个容器
                    striped: true,                      //是否显示行间隔色
                    cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                    pagination: true,                   //是否显示分页（*）
                    sortable: false,                     //是否启用排序
                    sortOrder: "asc",                   //排序方式
                    queryParams: oTableInit.queryParams,//传递参数（*）
                    sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
                    pageNumber: 1,                       //初始化加载第一页，默认第一页
                    pageSize: 10,                        //每页的记录行数（*）
                    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                    search: false,                       //是否显示表格搜索
                    // strictSearch: false, //是否启用全匹配搜索
                    showColumns: false,                  //是否显示所有的列
                    showRefresh: false,              //是否显示刷新按钮
                    // showPaginationSwitch: true,
                    minimumCountColumns: 2,             //最少允许的列数
                    clickToSelect: true,                //是否启用点击选中行
                    // height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                    uniqueId: "id",                     //每一行的唯一标识，一般为主键列
                    showToggle: false,                    //是否显示详细视图和列表视图的切换按钮
                    cardView: false,                    //是否显示详细视图
                    detailView: false,                   //是否显示父子表
                    clickToSelect: false,
                    showExport: false,
                    exportDataType: "basic",
                    //注册加载子表的事件。注意下这里的三个参数！
                    onExpandRow: function (index, row, $detail) {
                        oTableInit.InitSubTable(index, row, $detail);
                    },
                    onDblClickCell: function (field, value, row, $element) {
                        show(field, value, row, $element);
                    },
                    columns: [
                        {
                            title: '序号',
                            formatter: function (value, row, index) {
                                return index + 1;
                            },
                            sortable: true
                        }, {
                            field: 'stuname',
                            title: '下发人',
                        }, {
                            field: 'time_check',
                            title: '查看时间',
                        }, {
                            field: 'time_up',
                            title: '上交时间',
                        }, {
                            field: 'fileup',
                            title: '附件',
                            formatter: function (value, row, index) {
                                var a = "";
                                for (i = 0; i < value.length; i++) {
                                    a = a + value[i] + "<br>";
                                }
                                return a;
                            },
                            sortable: true
                        }, {
                            field: 'bz',
                            title: '备注',
                            sortable: true
                        }, {
                            field: 'tag',
                            title: '审核状态',
                            sortable: true
                        }, {
                            field: 'add_time',
                            title: '添加时间',
                            sortable: true
                        },
                    ]
                });
            };

            //得到查询的参数
            oTableInit.queryParams = function (params) {
                var temp = {   //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                    limit: params.limit,   //页面大小
                    offset: params.offset,  //页码
                    departmentname: $("#txt_search_departmentname").val(),
                    statu: $("#txt_search_statu").val()
                };
                return temp;
            };
            return oTableInit;
        };


        var ButtonInit = function () {
            var oInit = new Object();
            var postdata = {};

            oInit.Init = function () {
                //初始化页面上面的按钮事件

            };

            return oInit;
        };

    </script>
@endsection

