@extends('layouts.admin')

@section('nav')
    @include('admin/task/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/task/docheck')}}">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$id}}">
        <div class="form-group">
            <label for="show_tag" class="col-xs-12 col-sm-3 control-label no-padding-right">审核状态：</label>

            <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <div class="radio inline">
                       <label>
                           <input name="is_menu" type="radio" value="1" class="ace" checked/>
                           <span class="lbl"> 通过</span>
                       </label>
                    <label>
                           <input name="is_menu" type="radio" value="0" class="ace"/>
                           <span class="lbl"> 未通过</span>
                       </label>
                </div>
            </span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    {{--        引入jquery-validation--}}
    <script src="{{url('admin/assets/jquery-validation/lib/jquery.js')}}"></script>
    <script src="{{url('admin/assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
        $("#sample-form").validate({
            errorLabelContainer: "#xiaoxi",
            // 规则
            rules: {
                // 表单元素名称
                name: {
                    // 验证规则
                    required: true
                }
            },
            // 消息提示
            messages: {
                name: {
                    required: '请输入角色名称'
                }
            },
            // 取消键盘事件
            onkeyup: false,
            // 验证成功后的样式
            success: "valid",
            // 验证通过后，处理的方法 form dom对象
            submitHandler: function (form) {
                // 表单提交
                form.submit();
            }
        });
    </script>
    <script>

        var ctype_id = document.getElementById('ctype_id');
        document.addEventListener('click', function () {
            var route_class = document.getElementById('route_class');
            if (ctype_id.value != '0') {
                route_class.style.display = 'none';
            } else {
                route_class.style.display = 'block';
            }
        });


    </script>
@endsection
