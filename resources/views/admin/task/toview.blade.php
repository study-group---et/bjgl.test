@extends('layouts.admin')
@section('nav')
    @include('admin/task/nav')
@endsection
@section('cont')
    <div class="row">
        <div class="col-xs-12">
            <form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">标题：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->title}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">内容：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->cont}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">查看时间：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->time_check}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">截止时间：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->time_up}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">文件：</label>
                        <div class="col-sm-6" style="top:8px">
                            @foreach($task->fileup as $v)
                                {!! $v !!}<br>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">添加人：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->add_user}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">添加时间：</label>
                        <div class="col-sm-6" style="top:8px">
                            {{$task->add_time}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
