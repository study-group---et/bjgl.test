<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/task/index2">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            已审核列表
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/toview">
            已审核查看
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/create">
            已审核添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/check">
            已审核审核
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/task/{{ isset($task) ? $task->id : '' }}/update">
            已审核编辑
        </a>
    </li>
</ul>
