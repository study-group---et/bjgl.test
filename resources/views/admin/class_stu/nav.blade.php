<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/class_stu/index">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            学生列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/class_stu/create">
            学生添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/class_stu/{{ isset($stu) ? $stu->id : '' }}/update">
            学生编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/class_stu/{{ isset($stu) ? $stu->id : '' }}/auth">
            学生授权
        </a>
    </li>
</ul>
