<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/term/index">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            学期列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/term/create">
            学期添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/term/{{ isset($term) ? $term->id : '' }}/update">
            学期编辑
        </a>
    </li>
</ul>
