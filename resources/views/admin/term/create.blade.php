@extends('layouts.admin')

@section('nav')
    @include('admin/term/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/term/add')}}">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">学期名称：</label>
            <div class="col-sm-4">
                <input type="text" style="height: 35px;" required placeholder="" name="title">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">开始日期：</label>
            <div class="col-sm-4">
                <input autocomplete="off" type="text" name="time_start" id="test5" style="height: 35px;"
                       placeholder="yyyy-MM-dd">
                <script src="{$Think.config.super_public_url}/laydate/laydate.js"></script>
                <script>
                    laydate.render({
                        elem: '#test5'
                        , type: 'date'
                    });
                </script>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">结束日期：</label>
            <div class="col-sm-4">
                <input autocomplete="off" type="text" name="time_end" id="test6" style="height: 35px;"
                       placeholder="yyyy-MM-dd">
                <script>
                    laydate.render({
                        elem: '#test6'
                        , type: 'date'
                    });
                </script>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">备注：</label>
            <div class="col-sm-6">
                <input type="text" style="height: 35px;" placeholder="" name="bz" class="col-xs-5"/>
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">显示状态： </label>
            <div class="col-xs-3">
                <div class="radio">
                    <label>
                        <input name="show_tag" type="radio" value="1" checked class="ace">
                        <span class="lbl"> 显示 </span>
                    </label>
                    <label>
                        <input name="show_tag" type="radio" value="2" class="ace">
                        <span class="lbl"> 隐藏 </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">提交</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    {{--        引入jquery-validation--}}
    <script src="{{url('admin/assets/jquery-validation/lib/jquery.js')}}"></script>
    <script src="{{url('admin/assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
        $("#sample-form").validate({
            errorLabelContainer: "#xiaoxi",
            // 规则
            rules: {
                // 表单元素名称
                title: {
                    // 验证规则
                    required: true
                }
            },
            // 消息提示
            messages: {
                title: {
                    required: '请输入学期名称'
                }
            },
            // 取消键盘事件
            onkeyup: false,
            // 验证成功后的样式
            success: "valid",
            // 验证通过后，处理的方法 form dom对象
            submitHandler: function (form) {
                // 表单提交
                form.submit();
            }
        });
    </script>
@endsection
