@extends('layouts.admin')

@section('nav')
    @include('admin/term/nav')
@endsection

@section('cont')
    <form class="form-horizontal" id="sample-form" method="post" action="{{url('admin/term/edit',['id'=>$term->id])}}">
        {{ csrf_field() }}
        @csrf
        @method('PUT')
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">学期名称：</label>
            <div class="col-sm-4">
                <input type="text" style="height: 35px;" required placeholder="" value="{{$term->title}}" name="title">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">开始日期：</label>
            <div class="col-sm-4">
                <input autocomplete="off" type="text" value="{{$term->time_start}}" name="time_start" id="test5"
                       style="height: 35px;"
                       placeholder="yyyy-MM-dd">
                <script src="{$Think.config.super_public_url}/laydate/laydate.js"></script>
                <script>
                    laydate.render({
                        elem: '#test5'
                        , type: 'date'
                    });
                </script>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">结束日期：</label>
            <div class="col-sm-4">
                <input autocomplete="off" type="text" name="time_end" value="{{$term->time_over}}" id="test6"
                       style="height: 35px;"
                       placeholder="yyyy-MM-dd">
                <script>
                    laydate.render({
                        elem: '#test6'
                        , type: 'date'
                    });
                </script>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">备注：</label>
            <div class="col-sm-6">
                <input type="text" style="height: 35px;" placeholder="" value="{{$term->bz}}" name="bz"
                       class="col-xs-5"/>
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">显示状态： </label>
            <div class="col-xs-3">
                <div class="radio">
                    <label>
                        <input name="show_tag" type="radio" value="1" @if($term->tag==1) checked @endif class="ace">
                        <span class="lbl"> 显示 </span>
                    </label>
                    <label>
                        <input name="show_tag" type="radio" value="2" @if($term->tag==2) checked @endif class="ace">
                        <span class="lbl"> 隐藏 </span>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-5 col-sm-offset-4">
                <button class="btn btn-success" onclick="">修改</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-warning" type="reset">重置</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        //一、对姓名进行验证
        var oldxiaoxi;
        var oldclass;
        //姓名获取焦点时显示，内含失去焦点显示点击前样式及内容
        $("#name").focus(function () {
            oldxiaoxi = $("#xiaoxi").text();
            oldclass = $("#xiaoxi").attr("class");
            //修改css样式为黑色
            $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline");
            //修改HTML内容
            $("#xiaoxi").html("(正在输入...)");
            //当点击完输入框不做操作离开时避免显示正在输入，因此显示点击前所显示的内容及样式
            // $("#name").blur(function() {
            //     //修改css内容
            //     $("#xiaoxi").attr("class",oldclass);
            //     //修改HTML内容
            //     $("#xiaoxi").html(oldxiaoxi);
            // });
        });
        var oldname = "{{$term->name}}";
        var oldxiaoxi1;
        var oldclass1;
        //键盘离开时显示，内含失去焦点显示键盘离开后样式及内容.
        $("#name").keyup(function () {
            var name = $(" #name ").val();
            if (name === '') {
                //修改css样式为黑色
                $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline");
                //修改HTML内容
                $("#xiaoxi").html("(正在输入...)");
            } else if (name === oldname) {
                //修改css样式为黑色
                $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline green");
                //修改HTML内容
                $("#xiaoxi").html("(原用户名)");
            } else {
                //调用ajax
                $.ajax({
                    //请求方式
                    type: "post",

                    //服务器相应数据的解析方式
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //请求资源url
                    url: "/admin/term",
                    //此处需要同步处理，即ajax执行完后才能向下进行。（避免第一次点击输入框只有一次键盘触发时oldxiaoxi为（正在输入...））
                    // async: false,
                    //向服务器端发送的数据
                    data: {"name": name, "ajax": 'cunzai'},
                    success: function (data) {
                        $("#xiaoxi").attr("class", data.newclass);
                        $("#xiaoxi").html(data.cont);
                        oldxiaoxi1 = $("#xiaoxi").text();
                        oldclass1 = $("#xiaoxi").attr("class");
                    }
                });
            }

        });

        //失去焦点时的两种情况，1.获取焦点->失去焦点，2.获取焦点->键盘离开->失去焦点（最后一个else之后便是）
        $("#name").blur(function () {
            if ($(" #name ").val() === '') {
                //修改css样式为黑色
                $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                //修改HTML内容
                $("#xiaoxi").html("*必填");
            } else {
                if (oldxiaoxi1 === "(用户名已存在)") {
                    $("#name").val("");
                    //修改css内容
                    $("#xiaoxi").attr("class", "help-block col-xs-12 col-sm-reset inline red");
                    //修改HTML内容
                    $("#xiaoxi").html("*(必填)");
                } else if (oldxiaoxi1 === "(用户名可用)") {
                    // 修改css内容
                    $("#xiaoxi").attr("class", oldclass1);
                    //修改HTML内容
                    $("#xiaoxi").html(oldxiaoxi1);
                } else {
                    //修改css内容
                    $("#xiaoxi").attr("class", oldclass);
                    //修改HTML内容
                    $("#xiaoxi").html(oldxiaoxi);
                }
            }
        });

    </script>
@endsection
