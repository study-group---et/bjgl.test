<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/class/index">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            班级列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/class/create">
            班级添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/class/{{ isset($class) ? $class->id : '' }}/update">
            班级编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/class/{{ isset($class) ? $class->id : '' }}/auth">
            班级授权
        </a>
    </li>
</ul>
