@extends('layouts.admin')

@section('cont')
    <div class="page-header">
        <h1>
            超级管理员
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                首页
            </small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-block alert-success">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>

                <i class="ace-icon fa fa-check green"></i>

                欢迎使用
                <strong class="green">
                    班级大事纪后台管理系统
                    <small>(v1.4)</small>
                </strong>,
                您已登录
                <strong class="red">
                    {{ session('user.times') }}
                </strong>
                次，上次登录IP地址为：{{ session('user.last_ip') }}。
            </div>
        </div>
    </div>

@endsection
