@extends('layouts.admin')

@section('nav')
    @include('admin/manage/nav')
@endsection

@section('cont')
    <form action="{{ url('admin/manage/doauth') }}" class="form-horizontal" role="form" method="post">
        {{ csrf_field() }}

        <input type="hidden" name="manage_id" value="{{ $manage->id }}">

        <table id="simple-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <td style="text-align:left;" colspan="2">
                    用户名： {{ $manage->username }}
                </td>
            </tr>
            </thead>
            <tr>
                <td style="text-align:left;" colspan="2">
                    <div class="form-group">
                        <label for="show_tag" class="col-xs-12 col-sm-3 control-label no-padding-right">授权：</label>

                        <div class="col-xs-12 col-sm-5">
            <span class="block input-icon input-icon-right">
                <div class="radio inline">
                    @foreach($roleall as $v)
                        <label>
                           <input name="role_id" type="radio" value="{{$v->id}}" @if($v->id==$manage->role_id) checked
                                  @endif class="ace"/>
                           <span class="lbl"> {{$v->name}}</span>
                       </label>
                    @endforeach
                </div>
            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="100%" class="center">
                    <input type="submit" class="btn btn-info"></input>
                </td>
            </tr>
        </table>
    </form>

@endsection
