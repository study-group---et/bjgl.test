<ul class="nav nav-tabs">
    <li class="active">
        <a href="/admin/manage">
            <i class="green ace-icon fa fa-home bigger-120"></i>
            管理员列表
        </a>
    </li>

    <li class="hidden">
        <a href="/admin/manage/create">
            管理员添加
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/manage/{{ isset($manage) ? $manage->id : '' }}/edit">
            管理员编辑
        </a>
    </li>
    <li class="hidden">
        <a href="/admin/manage/{{ isset($manage) ? $manage->id : '' }}/auth">
            管理员授权
        </a>
    </li>
</ul>
