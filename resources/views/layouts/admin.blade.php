<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>班级大事纪后台管理系统</title>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    @include('admin.public.style')

    @yield('css')
</head>

<body class="no-skin">
@include('admin.public.navbar')
<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>
    @include('admin.public.left')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="{{url('admin/index')}}">首页</a>
                    </li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="tabbable">
                            @yield('nav')
                            <div class="tab-content">
                                @include('admin.public.msg')
                                @yield('cont')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.public.footer')
</div>


@yield('js')


<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='{{asset('admin/assets/js/jquery.min.js')}}'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='{{asset('admin/assets/js/jquery.mobile.custom.min.js')}}'>" + "<" + "/script>");
</script>
<script src="{{asset('admin/assets/js/bootstrap.min.js')}}"></script>

<!-- page specific plugin scripts -->

<!-- ace scripts -->
<script src="{{asset('admin/assets/js/ace-elements.min.js')}}"></script>
<script src="{{asset('admin/assets/js/ace.min.js')}}"></script>

<!-- inline scripts related to this page -->
<script src="{{asset('admin/assets/js/custom.js')}}"></script>

<script src="{{asset('admin/assets/js/jquery.cookie.js')}}"></script>

<!-- 好用的js日期选择器 -->
<script src="{{asset('admin/assets/Datepicker/js/foundation-datepicker.js')}}"></script>
<script src="{{asset('admin/assets/Datepicker/js/locales/foundation-datepicker.zh-CN.js')}}"></script>
<!-- <script src="{{asset('admin/assets/layer/layui.js')}}" type="text/javascript"></script> -->
<script src="{{asset('admin/assets/layer/layer.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.singleSelect').select2();
        // $("input").attr("autocomplete","off");
    });

    $('.dateformat').fdatepicker({
        format: 'yyyy.m.d',
    });
    $('.dateformat_').fdatepicker({
        format: 'yyyy-mm-dd',
    });
    $('.dateTimeFormat').fdatepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        pickTime: true
    });

    function del() {
        var msg = "您真的确定要删除吗？\n\n请确认！";
        if (confirm(msg) == true) {
            return true;
        } else {
            return false;
        }
    }

    function pingbi() {
        var msg = "您真的确定要屏蔽此账号吗？\n\n屏蔽后不可登录，请确认！";
        if (confirm(msg)) {
            return true;
        } else {
            return false;
        }
    }

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#dpd1').fdatepicker({
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.update(newDate);
        }
        checkin.hide();
        $('#dpd2')[0].focus();
    }).data('datepicker');
    var checkout = $('#dpd2').fdatepicker({
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');

    // 定位 针对navbar上的菜单
    // $('.dropdown-modal_').on('click', function () {
    // 	var href = $(this).find('.top_bar').attr('href');
    // 	$.cookie('link2', href, { expires: 7, path: '/' });
    // 	$.cookie('link', href, { expires: 7, path: '/' });

    // })

    // 定位 针对左侧的菜单
    $('#sidebar ul li a').on('click', function () {
        var href1 = $(this).attr('href');
        $.cookie('link2', href1, {expires: 7, path: '/'});
    })


    //checkbox 全选/取消全选
    var isCheckAll = false;

    function swapCheck() {
        if (isCheckAll) {
            $("input[type='checkbox']").each(function () {
                this.checked = false;
            });
            isCheckAll = false;
        } else {
            $("input[type='checkbox']").each(function () {
                this.checked = true;
            });
            isCheckAll = true;
        }
    }

    function previewImage(file, div, imghead, divhead) {
        var div = document.getElementById(div);
        if (file.files && file.files[0]) {
            div.innerHTML = '<img id="' + imghead + '">';
            var img = document.getElementById(imghead);
            img.onload = function () {
                var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
                img.width = rect.width;
                img.height = rect.height;
                // img.style.marginTop = rect.top+'px';
            }
            var reader = new FileReader();
            reader.onload = function (evt) {
                img.src = evt.target.result;
            }
            reader.readAsDataURL(file.files[0]);
        } else {
            var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
            file.select();
            var src = document.selection.createRange().text;
            div.innerHTML = '<img id="' + imghead + '">';
            var img = document.getElementById(imghead);
            img.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
            var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
            status = ('rect:' + rect.top + ',' + rect.left + ',' + rect.width + ',' + rect.height);
            div.innerHTML = "<div id='" + divhead + "' style='width:" + rect.width + "px;height:" + rect.height + "px;margin-top:" + rect.top + "px;" + sFilter + src + "\"'></div>";
        }
    }

    //select2
    $('.select2').css('width', '200px').select2({allowClear: true})
    $('#select2-multiple-style .btn').on('click', function (e) {
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        if (which == 2) $('.select2').addClass('tag-input-style');
        else $('.select2').removeClass('tag-input-style');
    });
</script>


</body>

</html>
