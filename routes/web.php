<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('class_user/{user}', 'ClassController@class_user');
    //登录视图
    Route::get('login', 'LoginController@login');
    Route::get('jiami', 'LoginController@jiami');
    //登录处理
    Route::post('dologin', 'LoginController@dologin');
    //退出登录
    Route::get('logout', 'LoginController@logout');
    //修改密码
    Route::get('password', 'LoginController@password');
    //修改密码
    Route::post('dopass', 'LoginController@dopass');
    Route::group(['middleware' => 'islogin'], function () {
        //后台首页
        Route::get('index', 'LoginController@index');
        //授权界面
        Route::get('manage/{id}/auth', 'ManageController@auth');
        //授权处理
        Route::post('manage/doauth', 'ManageController@doauth');
        //管理员主页数据
        Route::get('manage/data', 'ManageController@data');
        //管理员管理
        Route::resource('manage', 'ManageController');

        //学期模块相关路由
        Route::group(['prefix' => 'term'], function () {
            Route::get('index', 'TermController@Index');
            Route::get('data', 'TermController@data');
            Route::get('create', 'TermController@Create');
            Route::get('{id}/update', 'TermController@Update');
            Route::post('add', 'TermController@Add');
            Route::put('edit/{id}', 'TermController@Edit');
            Route::get('del/{id}', 'TermController@Del');
            Route::post('tag/{id}', 'TermController@tag');
        });
        //周次模块相关路由
        Route::group(['prefix' => 'week'], function () {
            Route::get('index', 'WeekController@Index');
            Route::get('data', 'WeekController@data');
            Route::get('create', 'WeekController@Create');
            Route::get('{id}/update', 'WeekController@Update');
            Route::post('add', 'WeekController@Add');
            Route::put('edit/{id}', 'WeekController@Edit');
            Route::get('del/{id}', 'WeekController@Del');
            Route::post('tag/{id}', 'WeekController@tag');
        });
        //班级模块相关路由
        Route::group(['prefix' => 'class'], function () {
            Route::get('index', 'ClassController@Index');
            Route::get('data', 'ClassController@data');
            Route::get('create', 'ClassController@Create');
            Route::get('{id}/update', 'ClassController@Update');
            Route::post('add', 'ClassController@Add');
            Route::put('edit/{id}', 'ClassController@Edit');
            Route::get('del/{id}', 'ClassController@Del');
            Route::post('tag/{id}', 'ClassController@tag');
        });
        //班级模块相关路由
        Route::group(['prefix' => 'class_stu'], function () {
            //授权界面
            Route::get('{id}/auth', 'StuController@auth');
            //授权处理
            Route::post('doauth', 'StuController@doauth');
            Route::get('index', 'StuController@Index');
            Route::get('data', 'StuController@data');
            Route::get('create', 'StuController@Create');
            Route::get('{id}/update', 'StuController@Update');
            Route::post('add', 'StuController@Add');
            Route::put('edit/{id}', 'StuController@Edit');
            Route::get('del/{id}', 'StuController@delete');
        });
        //班级大事纪模块相关路由
        Route::group(['prefix' => 'event'], function () {
            Route::get('index', 'EventController@Index');
            Route::get('data', 'EventController@data');
            Route::get('create', 'EventController@Create');
            Route::get('{id}/update', 'EventController@Update');
            Route::post('add', 'EventController@Add');
            Route::put('edit/{id}', 'EventController@Edit');
            Route::get('del/{id}', 'EventController@Del');
            Route::post('tag/{id}', 'EventController@tag');

        });
        Route::group(['prefix' => 'materials'], function () {
            Route::get('data/{id}','MaterialsController@data');
            Route::get('{id}/create', 'MaterialsController@index');
            Route::post('add/{id}', 'MaterialsController@add');
            Route::get('{id}/update', 'MaterialsController@update');
            Route::put('edit/{id}', 'MaterialsController@edit');
            Route::post('upload', 'MaterialsController@upload');
            Route::get('del/{id}', 'MaterialsController@Del');
        });

        //班级大事纪文件模块相关路由
        Route::group(['prefix' => 'eventfile'], function () {
            Route::get('index', 'EventFileController@Index');
            Route::get('data', 'EventFileController@data');
            Route::get('create', 'EventFileController@Create');
            Route::get('{id}/update', 'EventFileController@Update');
            Route::post('add', 'EventFileController@Add');
            Route::put('edit/{id}', 'EventFileController@Edit');
            Route::get('del/{id}', 'EventFileController@Del');
            Route::post('tag/{id}', 'EventFileController@tag');
            Route::post('upload', 'EventFileController@upload');
        });
        //加减分模块相关路由
        Route::group(['prefix' => 'points'], function () {
            Route::get('index', 'PointsController@Index');
            Route::get('index2', 'PointsController@Index2');
            Route::get('data', 'PointsController@data');
            Route::get('data2', 'PointsController@data2');
            Route::get('create', 'PointsController@Create');
            Route::get('{id}/update', 'PointsController@Update');
            Route::get('create2', 'PointsController@Create2');
            Route::get('{id}/update2', 'PointsController@Update2');
            Route::post('add', 'PointsController@Add');
            Route::put('edit/{id}', 'PointsController@Edit');
            Route::get('del/{id}', 'PointsController@Del');
            Route::post('tag/{id}', 'PointsController@tag');
            Route::post('classnmae', 'PointsController@classname');
        });
        //任务模块相关路由
        Route::group(['prefix' => 'task'], function () {
            Route::get('index', 'TaskController@Index');
            Route::get('index2', 'TaskController@Index2');
            Route::get('index6', 'TaskController@Index6');
            Route::get('data', 'TaskController@data');
            Route::get('{id}/check', 'TaskController@check');
            Route::post('docheck', 'TaskController@docheck');
            Route::get('data2/{id}', 'TaskController@data2');
            Route::get('data3', 'TaskController@data3');
            Route::get('data4', 'TaskController@data4');
            Route::get('data5/{id}', 'TaskController@data5');
            Route::get('data6', 'TaskController@data6');
            Route::get('{id}/details', 'TaskController@details');
            Route::get('create', 'TaskController@Create');
            Route::get('create2', 'TaskController@Create2');
            Route::get('{id}/update', 'TaskController@Update');
            Route::post('add', 'TaskController@Add');
            Route::put('edit/{id}', 'TaskController@Edit');
            Route::get('del/{id}', 'TaskController@Del');
            Route::post('tag/{id}', 'TaskController@tag');
            Route::post('tree', 'TaskController@tree');
            Route::post('upload', 'TaskController@upload');
            Route::get('show', 'TaskController@show');
            Route::get('{id}/end', 'TaskController@end');
            Route::post('doend', 'TaskController@doend');
            Route::get('{id}/toview', 'TaskController@toview');
            Route::get('{id}/toview2', 'TaskController@toview2');
            Route::get('{id}/toview3', 'TaskController@toview3');
            Route::get('doview/{id}', 'TaskController@doview');
        });
        //角色主页数据
        Route::get('role/data', 'RoleController@data');
        //角色授权
        Route::get('role/{id}/auth', 'RoleController@auth');
        //处理授权
        Route::post('role/doauth', 'RoleController@doauth');
        //删除
        Route::get('role/destroy/{id}', 'RoleController@destroy');
        //角色管理
        Route::resource('role', 'RoleController');


        //节点主页数据
        Route::get('node/data', 'NodeController@data');
        Route::get('node/data2', 'NodeController@data2');
        //修改
        Route::get('node/{id}/edit2', 'NodeController@edit2');
        //删除
        Route::get('role/destroy/{id}', 'RoleController@destroy');
        Route::get('node/destroy/{id}', 'NodeController@destroy');
        //节点管理
        Route::resource('node', 'NodeController');
    });

});
